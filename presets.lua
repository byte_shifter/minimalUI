mPreset_minimalUI = {
	["Bags"] = {
		["Item_Size"] = 27,
		["Items_Per_Row"] = 12,
		["BG_Color"] = {
			["a"] = 0.789158046245575,
			["b"] = 0.0470588235294118,
			["g"] = 0.0470588235294118,
			["r"] = 0.0470588235294118,
		},
		["Show_Bag_Slots"] = true,
		["Enabled"] = true,
	},
	["System"] = {
		["Enabled"] = true,
		["FX_Death_Effect"]    = false,
		["Pixel_Perfect_UI"]   = true,
		["Auto_Dismount"]      = true,
		["Show_Vendor_Prices"] = true,
	},
	["Minimap"] = {
		["Enabled"] = true,
		["Minimap_Width"] = 160,
		["Minimap_Icon_Side"] = "LEFT",
		["Minimap_Zone_Pos"] = "BOTTOM",
		["Minimap_Height"] = 160,
		["Minimap_Icon_Size"] = 20,
	},
	["Classbar"] = {
		["Enabled"] = true,
		["Cooldown_Tracker_Icon_Size"] = 26,
	},
}

MUI_DEFAULT_PRESET = mPreset_minimalUI