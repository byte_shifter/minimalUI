local system = mUI:RegisterModule("System", PRIORITY_SYSTEM)
system:RegisterEvents({"PLAYER_ENTERING_WORLD"})


-- QuestTimerFrame
function system:CalculateAndSetUIScale()
	if(mUI_VarGetValue("System", "Pixel_Perfect_UI")) then
		if(GetCVar("UseUIScale") == "1") then
			cprint("|cffffff00To have a pixel perfect ui experience we disabled UseUIScale in your Graphics Settings!")
			cprint("|cffffff00If you wish to NOT have this happen. Disable it in the /mui settings!")
			
			SetCVar("UseUIScale", false)
		end
		local Width, Height = wapiGetResolution()
		local RequiredScale = 768 / Height
		UIParent:SetScale(RequiredScale)
	end
end

function system:MakeBuiltinWindowsMoveable()
	local frames = { QuestTimerFrame, PlayerFrame, TargetFrame }
	for i, frame in ipairs(frames) do
		uiFrameRegisterDragging(frame)
	end
end

function system:SetShowVendorPrices(value)
	local function SetGameToolTipPrice(tt)
		local container = GetMouseFocus()
		if container and container.GetName then -- Auctionator sanity check
			-- price is already shown at vendor; still allow showing price for tradeskill items
			if not MerchantFrame:IsShown() or container:GetName():find("TradeSkill") then
				local itemLink = select(2, tt:GetItem())
				if itemLink then
					local itemSellPrice = select(11, GetItemInfo(itemLink))
					if itemSellPrice and itemSellPrice > 0 then
						local name = container:GetName()
						local object = container:GetObjectType()
						local count
						if object == "Button" then -- ContainerFrameItem, QuestInfoItem, PaperDollItem
							count = container.count
						elseif object == "CheckButton" then -- MailItemButton or ActionButton
							count = container.count or tonumber(container.Count:GetText())
						end
						local cost = (type(count) == "number" and count or 1) * itemSellPrice
						SetTooltipMoney(tt, cost, nil, SELL_PRICE .. ":")
					end
				end
			end
		end
	end

	local function SetItemRefToolTipPrice(tt)
		local itemLink = select(2, tt:GetItem())
		if itemLink then
			local itemSellPrice = select(11, GetItemInfo(itemLink))
			if itemSellPrice and itemSellPrice > 0 then
				SetTooltipMoney(tt, itemSellPrice, nil, SELL_PRICE .. ":")
			end
		end
	end

	if(value) then
		GameTooltip:HookScript("OnTooltipSetItem", SetGameToolTipPrice)
		ItemRefTooltip:HookScript("OnTooltipSetItem", SetItemRefToolTipPrice)		
	end
end

function system:SetAutoDismount(value)
	if(system.adhelper == nil) then
		system.adhelper = CreateFrame("Frame", "muiAutoDismountHelper", UIParent)
		system.adhelper:RegisterEvent("UI_ERROR_MESSAGE")
		system.adhelper:RegisterEvent("TAXIMAP_OPENED")
		system.adhelper.enabled = false
		system.adhelper.whitelist = { 
			50,  -- you are mounted
		}
				
		system.adhelper:SetScript("OnEvent", function(self, event, arg1, arg2, arg3)
			if(not self.enabled or not IsMounted()) then return end
			local dismount = false
			if(event == "TAXIMAP_OPENED") then
				dismount = true 
			elseif(event == "UI_ERROR_MESSAGE") then			
				if(mui_tcontains(self.whitelist, tonumber(arg1))) then
					dismount = true 
				end
			end
			if(dismount) then 
				Dismount()
			end
		end)
	end	
	system.adhelper.enabled = value
end

function system:OnVarChange(name, value)
	if(name == "Pixel_Perfect_UI") then
		if(value) then
			self:CalculateAndSetUIScale()
		end
		mUI_SetReloadRequired()
	elseif(name == "FX_Death_Effect") then
		SetCVar("ffxDeath", value)	
	elseif(name == "Auto_Dismount") then
		self:SetAutoDismount(value)
	elseif(name == "Show_Vendor_Prices") then
		if(not value) then ReloadUI() end
		self:SetShowVendorPrices(value)
	end
end

function system:OnEvent(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12)
	if(event == "PLAYER_ENTERING_WORLD")
	then
		-- Make sure we are pixel perfect at all times
		self:CalculateAndSetUIScale()
	end
end

function system:OnEnable()
	--self:CalculateAndSetUIScale()
end

function system:OnConfigShow()
	if(self.config.init == nil) then
		if(mUI_VarGenBegin(self.config, "System")) then
			mUI_VarGenBeginGroup(self.config, "Core Features")
				mUI_VarGenBoolean(self.config, "Pixel_Perfect_UI", "Pixel Perfect UI", 
				                  "Makes the user interface to look the same on any given resoultion\n\n** Reloads UI when changed!")
			mUI_VarGenEndGroup(self.config)
			
			mUI_VarGenBeginGroup(self.config, "Graphic Effects")
				mUI_VarGenBoolean(self.config, "FX_Death_Effect", "Death Effect Saturation", "Fullscreen saturation when your character is dead.")
			mUI_VarGenEndGroup(self.config)
			
			mUI_VarGenBeginGroup(self.config, "Helpers")
				mUI_VarGenBoolean(self.config, "Auto_Dismount", "Auto Dismount", "Automatically dismounts you on specific actions.")
				mUI_VarGenBoolean(self.config, "Show_Vendor_Prices", "Show Vendor Prices", "Shows vendor prices in the items tooltip.")
			mUI_VarGenEndGroup(self.config)
			mUI_VarGenEnd(self.config)
		end
	else
		self.config:Show()
	end	
end