local bags = mUI:RegisterModule("Bags")	
bags:RegisterEvents( 
{ 
	"BAG_UPDATE", 
	"BAG_UPDATE_COOLDOWN", 
	"BAG_UPDATE_DELAYED",
	"ITEM_LOCK_CHANGED", 
	"PLAYER_ENTERING_WORLD", 
	"BANKFRAME_OPENED", 
	"BANKFRAME_CLOSED",
	"PLAYER_MONEY",
	"PLAYER_TRADE_MONEY",
	"TRADE_MONEY_CHANGED",
	"SEND_MAIL_MONEY_CHANGED",
	"SEND_MAIL_COD_CHANGED",
	"TRIAL_STATUS_UPDATE",
	"PLAYER_LOGIN",
})
bags.tracker = {}
bags.tracker.current_copper_amount = 0
bags.tracker.earnings_since_login  = 0

local EXTENDED_ITEM_COLORS = {}
EXTENDED_ITEM_COLORS["Key"]   = { 0, 0.921, 0.909, 1.0 }
EXTENDED_ITEM_COLORS["Quest"] = { 0.8, 0.8, 0.2, 1.0 }

local MONEY_FRAME_TEXT_SIZE = 8

function bags:OnGlobalUnlock()
	if(self.bagframe) then		
		self:OpenBackpack()
		self.bagframe.search:ClearFocus()
		self.bagframe.search:EnableMouse(false)
		
		for index, bag in ipairs(self.bagframe.baghandler) do
			local b = self.bagframe.bags[bag.id]
			if(b) then
				for slot, item in ipairs(b.items) do
					item:EnableMouse(false)
				end
			end
		end
	end
end

function bags:OnGlobalLock()
	if(self.bagframe) then
		self.bagframe.search:EnableMouse(true)
		
		for index, bag in ipairs(self.bagframe.baghandler) do
			local b = self.bagframe.bags[bag.id]
			if(b) then
				for slot, item in ipairs(b.items) do
					item:EnableMouse(true)
				end
			end
		end
		
		self:CloseBackpack()
	end
end

function bags:OpenBank()
	if(bags.bankframe) then
		if(not bags.bankframe:IsVisible()) then
			bags.bankframe:ClearAllPoints()
			bags.bankframe:SetPoint("TOPRIGHT", bags.bagframe, "TOPLEFT", -5, 0)
			bags.bankframe:Show()
			bags.bankframe:FullUpdate(true)
		end
	end
end

function bags:CloseBank()
	if(bags.bankframe) then
		if(bags.bankframe:IsVisible()) then
			bags.bankframe:Hide()
		end
	end
end

function bags:ToggleBackpack()
	if(bags.bagframe) then
		if bags.bagframe:IsVisible() then
			bags.bagframe:Hide()
		else
			bags.bagframe:Show()
		end
	end
end

function bags:OpenBackpack()
	if(bags.bagframe) then
		if(not bags.bagframe:IsVisible()) then
			bags.bagframe:Show()
			bags.bagframe:FullUpdate(true)
		end
	end
end

function bags:CloseBackpack()
	if(bags.bagframe) then
		if(bags.bagframe:IsVisible()) then
			bags.bagframe:Hide()
		end
	end
end

function bags:OpenAllBags()
	if(bags.bagframe) then
		if(not bags.bagframe:IsVisible()) then
			bags.bagframe:Show()
		end
	end
end

function bags:CloseAllBags()
	if(bags.bagframe) then
		if(bags.bagframe:IsVisible()) then
			bags.bagframe:Hide()
		end
	end
end

function bags:OnVarChange(name, value)
	if(self.bagframe) then
		self.bagframe:FullUpdate(true)		
	end
	
	if(self.bankframe) then
		self.bankframe:FullUpdate(true)
	end
end

local function InventoryFrameSearchOnTextChanged(self) 
	-- straight forward stupid search
	local pattern = self:GetText()
	if(pattern ~= "Search") then
		self:GetParent().search_enabled = true
	else
		self:GetParent().search_enabled = false
	end
	
	if(self:GetParent().search_enabled) then 
		for index, bag in ipairs(self:GetParent().baghandler) do
			for slot, item in ipairs(bag.items) do
				if(item.name and pattern ~= "") then
					local startPos, endPos, firstWord, restOfString = strfind(strlower(item.name), strlower(pattern))
					if(startPos) then
						item.match = true
					else
						item.match = false
					end
				else
					item.match = false
				end
			end
		end
	end
	self:GetParent():FullUpdate(true)
end

local function InventoryFrameSearchOnEscapePressed(self)
	self:SetText("Search") 
	self:ClearFocus()
	self:HighlightText(0,0) 
end

local function InventoryFrameSearchOnEditFocusGained(self)
	if(self:GetText() == "Search") then 
		self:SetText("") 
	end 
	self:HighlightText()
end

local function InventoryFrameSearchOnEditFocusLost(self)
	if(self:GetText() == "") then 
		self:SetText("Search") 
	end 
end

local function InventoryCreateFrameTemplate(parent, name, bag_ids)
	local frame = uiCreateElement(parent, "Frame", name)
	frame:SetPoint("RIGHT", parent, "RIGHT", -15, 0)
	frame:SetFrameStrata("LOW")	
	frame:SetShadowColor(0,0,0,0)
	
	frame.info = uiFontString(frame, frame:GetName().."InfoText")
	frame.info:ClearAllPoints()
	frame.info:SetPoint("TOPLEFT", frame, "TOPLEFT", 4, -2)
	
	frame.money = CreateFrame("Frame", frame:GetName().."MoneyFrame", frame)
	--frame.money = uiCreateElement(frame, "Frame", frame:GetName().."MoneyFrame")
	frame.money:SetPoint("TOPRIGHT", frame, "TOPRIGHT", -12, -2)
	frame.money:SetWidth(164)
	frame.money:SetHeight(16)
	
	--frame.money:SetBackgroundColor(1,0,0,1)
	frame.money.text = uiFontString(frame.money, "", nil, nil, nil, nil, "CENTER", "RIGHT")
	frame.money:SetScript("OnEnter", function(self)
		local IsNegative = false
		local Money = bags.tracker.earnings_since_login
		if(Money < 0) then 
			Money = Money * -1 
			IsNegative = true
		end
		
		local EarningsSinceLoginText = GetCoinTextureString(Money, 10)
		if(IsNegative) then 
			EarningsSinceLoginText = "|cffffffffEarnings session: |cffcc2222-".. EarningsSinceLoginText
		else
			EarningsSinceLoginText = "|cffffffffEarnings session: |cffffffff".. EarningsSinceLoginText
		end
		GameTooltip:SetOwner(self, "ANCHOR_CURSOR");
		GameTooltip:SetPoint("BOTTOMLEFT", self, "TOPLEFT", 0, 2)
		GameTooltip:SetText(EarningsSinceLoginText)
		GameTooltip:Show()
	end)
	frame.money:SetScript("OnLeave", function (self)
		GameTooltip:Hide()
	end)
	
	local search = uiCreateTextEdit(frame, "Search", "Search")	
	search:SetPoint("TOPLEFT", frame, "TOPLEFT", 0, 0)
	search:SetBackgroundColor(0,0,0,0.5)
	search:SetScript("OnTextChanged", InventoryFrameSearchOnTextChanged)
	search:SetScript("OnEscapePressed", InventoryFrameSearchOnEscapePressed)
	search:SetScript("OnEditFocusGained", InventoryFrameSearchOnEditFocusGained)
	search:SetScript("OnEditFocusLost", InventoryFrameSearchOnEditFocusLost)
	frame.search = search
	
	frame.baghandler = uiGetBagHandler(unpack(bag_ids))
	frame.bags = {}
	for index, bag in ipairs(frame.baghandler) do
		local bagframe = frame.bags[bag.id]
		if(bagframe == nil) then
			local template = "ContainerFrameItemButtonTemplate"
			local name = frame:GetName()
			if(bag.id == -1) then
				template = "BankItemButtonGenericTemplate"
			elseif(bag.id >= 0 and bag.id <= 4) then
				name = name .. bag.id
			else 
				name = name .. "Slot" .. bag.id
			end
			
			bagframe = CreateFrame("Frame", name, frame)			
			bagframe:SetID(bag.id)
			bagframe.ref 	= bag
			bagframe.items 	= {}
			bagframe.DoUpdate = function(self)
				self.ref:Update()

				local max = max(self.ref.slots, getn(self.items))
				for slot=1, max do
					local iteminfo = self.ref.items[slot]
					local item = self.items[slot]
					
					if(item == nil) then
						item = uiCreateElement(self, "Button", "Item"..slot, 32, 32, template)
						local q = { item:GetRegions() }
						for i, r in ipairs(q) do
							r:Hide()
							r:SetAlpha(0)
						end
						item:SetID(slot)
						item:SetFrameLevel(5)
						item:ClearAllPoints()
						
						local count = get_global(item:GetName().."Count")
						count:SetFontObject(MUIDEFAULTFONT)
						count:ClearAllPoints()
						count:SetAllPoints(item)
						count:SetJustifyV("BOTTOM")
						count:SetJustifyH("RIGHT")
						count:Show()
						count:SetAlpha(1)
											
						local h = item:CreateTexture(nil, "HIGHLIGHT")
						h:SetAllPoints(item)
						h:SetTexture("Interface\\AddOns\\minimalUI\\img\\Glow.tga")
						h:SetVertexColor(.4,.4,.4,.5)
						h:SetBlendMode("ADD")
	
						uiAddCooldownFrame(item, COOLDOWN_CONTAINER, self:GetID(), slot)
						self.items[slot] = item
					end

					local icon = get_global(item:GetName().."IconTexture")
					icon:SetAllPoints(item)
					if(iteminfo.texture) then
						icon:SetTexture(iteminfo.texture)
						icon:SetTexCoord(.075, .925, .075, .925)
						icon:SetAlpha(1.0)
						icon:Show()
					else
						icon:SetTexture(nil)
					end
					
					if(iteminfo.locked)  
					then
						icon:SetDesaturated(true)
					else
						icon:SetDesaturated(false)
					end

					SetItemButtonCount(item, iteminfo.count or 0)

					uiCooldownUpdate(item)

					local quality = iteminfo.quality or 0
					item:SetBackgroundColor(0.0,0.0,0.0,0.5)
					item:SetShadowColor(0.0, 0.0, 0.0, 1.0)
					item:SetBorderColor(0.0, 0.0, 0.0, 1.0)
					if(iteminfo.name) then
						local qcol = ITEM_QUALITY_COLORS[quality]
						if(quality > 1) 
						then
							item:SetShadowColor(qcol.r, qcol.g, qcol.b, .6)
							item:SetBorderColor(qcol.r, qcol.g, qcol.b, .6)
						else
							local color = EXTENDED_ITEM_COLORS[iteminfo.type]
							if(color) then
								item:SetBorderColor(unpack(color))
								item:SetShadowColor(unpack(color))
							end
						end
						
						
					end	
					
					if(frame.search_enabled) then
						if(iteminfo.match) then
							UIFrameFadeIn(item, .25, item:GetAlpha(), 1.0)
							--item:SetBorderColor(0.078, 1, 0.874,1.0)
							--item:SetShadowColor(0.078, 1, 0.874,1.0)
						else
							UIFrameFadeOut(item, .25, item:GetAlpha(), 0.15)
						end
					else
						UIFrameFadeIn(item, .25, item:GetAlpha(), 1.0)
					end

					item:Show()
				end
			end

			frame.bags[bag.id] = bagframe
		end
	end
	
	frame.FullUpdate = function(self, force_update)
		if(not self:IsVisible() and not force_update) then return end
		
		local outer_padding  = 3
		local item_padding   = 1
		local item_size  	 = mUI_VarGetValue("Bags", "Item_Size")
		local items_per_row  = mUI_VarGetValue("Bags", "Items_Per_Row")
		local bg_col 		 = mUI_VarGetValue("Bags", "BG_Color")
		local show_bags_slot = mUI_VarGetValue("Bags", "Show_Bag_Slots")

		local cp = 1
		local cy = -(outer_padding) - 16
		local cx = (outer_padding)

		local max_slots = 0
		local free_slots = 0
				
		for index, bag in mui_spairs(self.bags) do
			if(not layout_only) then 
				bag:DoUpdate() 
			end
			
			local show = true
			local bag_slots = bag.ref.slots
			local num_slots = max(bag_slots, getn(bag.items))
			for slot=1, num_slots do
				if(show and slot <= bag_slots) then
					bag.items[slot]:SetHeight(item_size)
					bag.items[slot]:SetWidth(item_size)
					bag.items[slot]:SetPoint("TOPLEFT", self, "TOPLEFT", cx, cy)
					bag.items[slot]:Show()
					
					if(mod(cp, items_per_row) == 0) then
						cx = (outer_padding)
						cy = cy - item_size - item_padding
					else
						cx = cx + item_size + item_padding
					end		
					cp = cp + 1
				else
					bag.items[slot]:Hide()
				end
			end
		end

		if(mod(cp - 1, items_per_row) ~= 0) then 
			cy = cy - item_size - item_padding 
		end
		
		self:SetHeight(-cy + outer_padding + 16)
		self:SetWidth(((item_size + item_padding) * items_per_row) - item_padding + outer_padding * 2)
		self:SetBackgroundColor(bg_col.r, bg_col.g, bg_col.b, bg_col.a)

		self.search:SetPoint("TOPLEFT", self, "BOTTOMLEFT", outer_padding, outer_padding + 16)
		self.search:SetPoint("BOTTOMRIGHT", self, "BOTTOMRIGHT", -outer_padding, outer_padding)
		
		local slots, slots_free = self.baghandler:GetSlotCount()
		self.info:SetText("Capacity: "..slots-slots_free.."/"..slots)
		
		if(self.bagslotsframe) then
			self.bagslotsframe:SetBackgroundColor(bg_col.r, bg_col.g, bg_col.b, bg_col.a)
			if(show_bags_slot) then
				self.bagslotsframe:Show()
			else
				self.bagslotsframe:Hide()
			end
		end
		
	end
	frame:FullUpdate()
	frame:Hide()
	return(frame)
end

local BAGTYPE_PLAYER_BAGS = 1
local BAGTYPE_PLAYER_BANK = 2
local function InventoryCreateFrame(bagtype)
	-- Temporary bag slot display to make it easier to swap bags in the inventory!
	local function BagSlotOnEnterHook(self, bagframe, bagid)
		if(bagframe:IsVisible()) then
			local bagid_slot = bagid
			bagframe.search_enabled = true
			
			for i, bag in ipairs(bagframe.baghandler) do
				if(bag.id == bagid_slot) then
					for j, item in pairs(bag.items) do
						if(item) then 
							item.match = true
						else 
							item.match = false
						end
					end
				end
			end
			
			bagframe:FullUpdate()
		end
	end
	
	local function BagSlotOnLeaveHook(self, bagframe, bagid)
		if(bagframe:IsVisible()) then
			local bagid_slot = bagid
			bagframe.search_enabled = false
			
			for i, bag in ipairs(bagframe.baghandler) do
				if(bag.id == bagid_slot) then
					for j, item in pairs(bag.items) do
						item.match = false
					end
				end
			end
			
			bagframe:FullUpdate()
		end
	end

	if(bagtype == BAGTYPE_PLAYER_BAGS) then
		local frame = InventoryCreateFrameTemplate(UIParent, "PlayerInventoryBag", {0, 1, 2, 3, 4})
		frame:RegisterGlobalDragging()
				
		frame.bagslotsframe = uiCreateElement(frame, "Frame", frame:GetName() .. "BagSlotsFrame")
		frame.bagslotsframe.slots = {}
		local bagslotsframe_width = 0
		for i=0,3 do
			local bag = get_global("CharacterBag"..i.."Slot")
			if(bag) then
				local slot = uiCreateElement(frame.bagslotsframe, "Frame", frame.bagslotsframe:GetName() .. "Bag"..i, 27, 27)
								
				bag:SetParent(slot)
				bag:SetAllPoints(slot)
				bag:SetFrameLevel(3)
				local q = { bag:GetRegions() }
				for i, r in ipairs(q) do
					r:Hide()
					r:SetAlpha(0)
				end
				
				local icon = get_global(bag:GetName() .. "IconTexture")
				if(icon) then
					icon:SetPoint("TOPLEFT", slot, "TOPLEFT", 1, -1)
					icon:SetPoint("BOTTOMRIGHT", slot, "BOTTOMRIGHT", -1, 1)
					icon:SetAlpha(1.0)
					icon:SetTexCoord(.075, .925, .075, .925)
				end			
				
				local h = bag:CreateTexture(nil, "HIGHLIGHT")
				h:SetAllPoints(bag)
				h:SetTexture("Interface\\AddOns\\minimalUI\\img\\Glow.tga")
				h:SetVertexColor(.4,.4,.4,.5)	
				h:SetBlendMode("ADD")
							
				if(i == 0) then
					slot:SetPoint("TOPRIGHT", frame.bagslotsframe, "TOPRIGHT", -2, -2)
				else
					slot:SetPoint("TOPRIGHT", frame.bagslotsframe.slots[i-1], "TOPLEFT", -1, 0)
				end
				
				bagslotsframe_width = bagslotsframe_width +  slot:GetWidth() + 1
				
				frame.bagslotsframe.slots[i] = slot
				
				
				bag:HookScript("OnEnter", function(self) BagSlotOnEnterHook(self, frame, i+1) end)
				bag:HookScript("OnLeave", function(self) BagSlotOnLeaveHook(self, frame, i+1) end)
			end
			
			MainMenuBarBackpackButton:HookScript("OnEnter", function(self) BagSlotOnEnterHook(self, frame, 0) end)
			MainMenuBarBackpackButton:HookScript("OnLeave", function(self) BagSlotOnLeaveHook(self, frame, 0) end)
		end
		
		frame.bagslotsframe:SetWidth(bagslotsframe_width + 3)
		frame.bagslotsframe:SetHeight(31)
		frame.bagslotsframe:SetPoint("BOTTOMRIGHT", frame, "TOPRIGHT", 0, 1) 
		
		frame:SetScript("OnShow", function(self) 
			if(mUI_VarGetValue("Bags", "Show_Bag_Slots") == true) then
				frame.bagslotsframe:Show()
			else 
				frame.bagslotsframe:Hide()
			end
		end)
		
		return frame
	elseif(bagtype == BAGTYPE_PLAYER_BANK) then
		local frame = InventoryCreateFrameTemplate(UIParent, "PlayerInventoryBank", {-1, 5, 6, 7, 8, 9, 10})
		frame.money:Hide()		
		
		frame.bagslots = {}
		for i=1,6 do
			local bag = BankSlotsFrame["Bag"..i]
			if(bag) then
				bag:HookScript("OnEnter", function(self) BagSlotOnEnterHook(self, frame, i+4) end)
				bag:HookScript("OnLeave", function(self) BagSlotOnLeaveHook(self, frame, i+4) end)			
			end	
		end
		
		return frame
	end
end

function bags:OnEnable()
	self.bagframe = InventoryCreateFrame(BAGTYPE_PLAYER_BAGS)
	self.bankframe = InventoryCreateFrame(BAGTYPE_PLAYER_BANK)
	
	-- override global bag callbacks
	ToggleBackpack 	= self.ToggleBackpack
	OpenBackpack    = self.OpenBackpack
	CloseBackpack   = self.CloseBackpack
	OpenAllBags     = self.OpenAllBags
	CloseAllBags 	= self.CloseAllBags
	ToggleBag       = function() end
	OpenBag         = function() end
	
	--mUI_VarRegister("Bags", bags, HandleVarChange)
end

function bags:OnConfigShow()
	if(self.config.init == nil) then
		if(mUI_VarGenBegin(self.config, "Bags")) then
			mUI_VarGenBeginGroup(self.config, "Visual", {0.63, 0.41, 0.27, .9})
				mUI_VarGenColor(self.config, "BG_Color", "Background Color")
				mUI_VarGenNumberField(self.config, "Item_Size",  "Item Size")
				mUI_VarGenNumberField(self.config, "Items_Per_Row",  "Items per Row")
				mUI_VarGenBoolean(self.config, "Show_Bag_Slots", "Show Bag Slots")
			mUI_VarGenEndGroup(self.config)
			mUI_VarGenEnd(self.config)
		end
	else
		self.config:Show()
	end	
	self:OpenAllBags()
end

function bags:OnConfigHide()
	self:CloseAllBags()
end

function bags:OnEvent(event, arg1, arg2)
	if(event == "BAG_UPDATE_DELAYED" or event == "ITEM_LOCK_CHANGED" or event == "BAG_UPDATE" ) then
		self.bagframe:FullUpdate(true)
		self.bankframe:FullUpdate(true)
	elseif(event == "PLAYER_ENTERING_WORLD" or event == "BAG_UPDATE_COOLDOWN") then
		self.bagframe.money.text:SetText(GetCoinTextureString(GetMoney(), MONEY_FRAME_TEXT_SIZE))
		self.bagframe:FullUpdate(true)
		self.bankframe:FullUpdate(true)
	elseif(event == "BANKFRAME_OPENED") then
		self:OpenAllBags()
		self:OpenBank()
	elseif(event == "BANKFRAME_CLOSED") then
		self:CloseAllBags()
		self:CloseBank()
	elseif( event == "PLAYER_MONEY" or 
			event == "PLAYER_TRADE_MONEY" or
			event == "TRADE_MONEY_CHANGED" or
			event == "SEND_MAIL_MONEY_CHANGED" or
			event == "SEND_MAIL_COD_CHANGED" or
			event == "TRIAL_STATUS_UPDATE") 
	then
		local money = GetMoney()
		self.tracker.earnings_since_login = self.tracker.earnings_since_login + (money - self.tracker.current_copper_amount)
		self.tracker.current_copper_amount = money
		self.bagframe.money.text:SetText(GetCoinTextureString(self.tracker.current_copper_amount, MONEY_FRAME_TEXT_SIZE))
	elseif(event == "PLAYER_LOGIN") then
		self.tracker.current_copper_amount = GetMoney()
		self.tracker.earnings_since_login  = 0
	end
end