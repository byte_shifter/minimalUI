local minimap = mUI:RegisterModule("Minimap")
minimap:RegisterEvents({"PLAYER_ENTERING_WORLD"})

function minimap:ScanMinimapAndAddThirdPartyIconIfExists()	
	local children = { Minimap:GetChildren() }
	for i, child in pairs(children) do
		if(strfind(child:GetName(), "LibDBIcon10")) then
			if(not Minimap.icondock:HasIcon(child)) then
				local i0, i1, i2, icon = child:GetRegions()
				i0:Hide()
				i1:Hide()
				i2:Hide()

				local bg = child:CreateTexture(nil)
				bg:SetDrawLayer("BACKGROUND")
				bg:SetColorTexture(1,1,1,1)
				bg:SetVertexColor(unpack(UI_DEFAULT_BG_COLOR))
				bg:SetAllPoints(child)
				uiAddThinBorder(child)
				
				
				icon:SetDrawLayer("ARTWORK")
				icon:SetBlendMode("BLEND")
				icon:SetTexCoord(.1, .9, .1, .9)
				icon:SetAllPoints(child)
				
				child:RegisterForDrag()

				Minimap.icondock:AddIcon(child)
			end
		end
	end
end

function minimap:OnEnable()
	-- Create a dock for all minimap icons
	Minimap.icondock = uiCreateElement(Minimap, "Frame", "IconDockRegion", 24, 24)
	Minimap.icondock:SetAlpha(0.0)
	Minimap.icondock.icons 		= {}
	Minimap.icondock.padding    = 0
	Minimap.icondock.iconsize 	= 24
	Minimap.icondock.dockside 	= "LEFT"
	Minimap.icondock.LayoutIcons = function(self)
		if(self.dockside == "LEFT") then
			self:SetPoint("TOPLEFT", Minimap, "TOPLEFT", -self.iconsize - 4, 2)
		elseif(self.dockside == "RIGHT") then
			self:SetPoint("TOPLEFT", Minimap, "TOPRIGHT", 4, 2)
		end

		local idx = 0
		for i, frame in pairs(self.icons) do
			if(frame:IsVisible()) then
				frame:SetWidth(self.iconsize)
				frame:SetHeight(self.iconsize)
				frame:SetPoint("TOPLEFT", self, "TOPLEFT", 0, -(idx * (self.iconsize+1) ) )
				idx = idx + 1
			end
		end
	end
	
	Minimap.icondock.AddIcon = function(self, frame)
		table.insert(self.icons, frame)
		frame:HookScript("OnShow", function() self:LayoutIcons() end)
		frame:HookScript("OnHide", function() self:LayoutIcons() end)
		self:LayoutIcons()
	end
	Minimap.icondock.HasIcon = function(self, frame)
		return mui_tcontains(self.icons, frame)
	end
	
	Minimap:SetMaskTexture("Interface\\AddOns\\minimalUI\\img\\BackdropSolid")
	Minimap:SetHeight(mUI_VarGetValue("Minimap", "Minimap_Width"))
	Minimap:SetWidth(mUI_VarGetValue("Minimap", "Minimap_Height"))
	uiFrameRegisterGlobalDragging(Minimap)
	Minimap:EnableMouse(true)
	
	local MinimapFrame = uiCreateElement(Minimap, "Frame", "MinimapFrame")
	Minimap:SetFrameLevel(2)
	MinimapFrame:SetFrameLevel(1)
	MinimapFrame:SetPoint("TOPLEFT", Minimap, "TOPLEFT", -2, 2)
	MinimapFrame:SetPoint("BOTTOMRIGHT", Minimap, "BOTTOMRIGHT", 2, -2)
	
	local zone_frame = uiCreateElement(Minimap, "Frame", "MinimapZoneFrame")
	zone_frame:RegisterEvent("ZONE_CHANGED")
	zone_frame:RegisterEvent("ZONE_CHANGED_INDOORS")
	zone_frame:RegisterEvent("ZONE_CHANGED_NEW_AREA")
	zone_frame:RegisterEvent("PLAYER_ENTERING_WORLD")
	zone_frame:SetHeight(16)
	zone_frame:SetScript("OnEvent", function(this) this.zone_changed = true end)
	zone_frame.text = uiFontString(zone_frame, "Zone", nil, nil, nil, nil, nil, nil, nil, true)
	zone_frame.text:SetAllPoints(zone_frame)
	zone_frame:SetScript("OnUpdate", function(this)
   	    if(this.zone_changed) then
   	    	this.text:SetText(GetMinimapZoneText());
   	    	this.zone_changed = false
   	    end
    end)
	Minimap.zoneframe = zone_frame
end

function minimap:OnVarChange(name, value)
	if(name == "Minimap_Icon_Side") then
		Minimap.icondock.dockside = value
		Minimap.icondock:LayoutIcons()
	elseif(name == "Minimap_Icon_Size") then
		Minimap.icondock.iconsize = value
		Minimap.icondock:LayoutIcons()
	elseif(name == "Minimap_Width") then
		Minimap:SetWidth(value)
	elseif(name == "Minimap_Height") then
		Minimap:SetHeight(value)
	elseif(name == "Minimap_Zone_Pos") then
		if(value == "BOTTOM") then
			Minimap.zoneframe:ClearAllPoints()
			Minimap.zoneframe:SetPoint("TOPLEFT", Minimap, "BOTTOMLEFT", -2, -3)
			Minimap.zoneframe:SetPoint("TOPRIGHT", Minimap, "BOTTOMRIGHT", 2, -3)
		else
			Minimap.zoneframe:ClearAllPoints()
			Minimap.zoneframe:SetPoint("BOTTOMLEFT", Minimap, "TOPLEFT", -2, 3)
			Minimap.zoneframe:SetPoint("BOTTOMRIGHT", Minimap, "TOPRIGHT", 2, 3)
		end
	end
end

function minimap:OnEvent(event, arg1)
	if(not Minimap.icondock:HasIcon(MiniMapMailFrame)) then
		-- Hide Blizzard textures	
		MinimapBackdrop:Hide()
		MinimapZoneTextButton:Hide()
		MinimapToggleButton:Hide()
		MinimapBorderTop:Hide()
		MinimapZoomIn:Hide()
		MinimapZoomOut:Hide()
		MiniMapWorldMapButton:Hide()
		--TimeManagerClockButton:Hide()
		GameTimeFrame:Hide()

		-- Skin and Arrange Blizzard Minimap Icons
		MiniMapMailBorder:Hide()
		MiniMapMailFrame:ClearAllPoints()
		MiniMapMailIcon:SetTexCoord(.17, .99, .1, .95)
		MiniMapMailIcon:SetAllPoints(MiniMapMailFrame)
		Minimap.icondock:AddIcon(MiniMapMailFrame)
		uiAddThinBorder(MiniMapMailFrame)
		
		MiniMapTrackingBorder:Hide()
		MiniMapTrackingIcon:SetTexCoord(.1, .9, .1, .9)
		MiniMapTrackingIcon:SetAllPoints(MiniMapTrackingFrame)
		Minimap.icondock:AddIcon(MiniMapTrackingFrame)
		uiAddThinBorder(MiniMapTrackingFrame)

		MiniMapBattlefieldBorder:Hide()
		MiniMapBattlefieldFrame:ClearAllPoints()
		MiniMapBattlefieldIcon:SetTexCoord(.3, .7, .3, .7)
		MiniMapBattlefieldIcon:SetAllPoints(MiniMapBattlefieldFrame)
		Minimap.icondock:AddIcon(MiniMapBattlefieldFrame)
		uiAddThinBorder(MiniMapBattlefieldFrame)
		
		local bg, ticker, fired = TimeManagerClockButton:GetRegions()
		bg:Hide()
		local b = uiCreateElement(TimeManagerClockButton, "Frame", "Background")
		b:ClearAllPoints()
		b:SetPoint("TOPLEFT", ticker, "TOPLEFT", -2, 2)
		b:SetPoint("BOTTOMRIGHT", ticker, "BOTTOMRIGHT", 2, -2)
		b:SetFrameLevel(4)
		
		TimeManagerClockButton:SetPoint("TOP", 0, 3)
		ticker:SetFontObject(MUIDEFAULTFONT)
		
		local btn_character = uiCreateToggleButton(Minimap, "CharacterFrameButton", 20, 20, mui_color2chat(MUICOLORS[1]) .. "C", function() ToggleCharacter("PaperDollFrame") end)
		btn_character:SetPoint("TOPLEFT", Minimap, "BOTTOMLEFT", -2, -3)
		
		local btn_spells = uiCreateToggleButton(Minimap, "SpellBookButton", 20, 20, mui_color2chat(MUICOLORS[2]) .. "S", function() ToggleFrame(SpellBookFrame) end)
		btn_spells:SetPoint("LEFT", btn_character, "RIGHT", 2, 0)
		
		local btn_talents = uiCreateToggleButton(Minimap, "TalentsButton", 20, 20, mui_color2chat(MUICOLORS[3]) .. "T", function() ToggleTalentFrame() end)
		btn_talents:SetPoint("LEFT", btn_spells, "RIGHT", 2, 0)
		
		local btn_quests = uiCreateToggleButton(Minimap, "QuestLogButton", 20, 20, mui_color2chat(MUICOLORS[4]) .. "Q", function() ToggleFrame(QuestLogFrame) end)
		btn_quests:SetPoint("LEFT", btn_talents, "RIGHT", 2, 0)
	
		local btn_social = uiCreateToggleButton(Minimap, "SocialButton", 20, 20, mui_color2chat(MUICOLORS[5]) .. "F", function() ToggleFriendsFrame() end)
		btn_social:SetPoint("LEFT", btn_quests, "RIGHT", 2, 0)
	
		--local btn_worldmap = uiCreateToggleButton(Minimap, "WorldMapButton", 20, 20, mui_color2chat(MUICOLORS[6]) .. "W", function() ToggleFrame(WorldMapFrame) end)
		--btn_worldmap:SetPoint("LEFT", btn_social, "RIGHT", 2, 0)
		
		local btn_help  = uiCreateToggleButton(Minimap, "SocialButton", 20, 20, mui_color2chat(MUICOLORS[6]) .. "?", function() ToggleHelpFrame() end)
		btn_help:SetPoint("LEFT", btn_social, "RIGHT", 2, 0)
	end
	
	self:ScanMinimapAndAddThirdPartyIconIfExists()

	local point, relativeTo, relativePoint, xOfs, yOfs = Minimap:GetPoint(1)
	if(point == "CENTER" and relativePoint == "TOP" and xOfs == 9 and yOfs == -92) then
		-- Minimap is in the default classic wow position but since we modified it we need to slightly move it!
		Minimap:SetPoint("TOPRIGHT", -5, -21)
	end
end

function minimap:OnConfigShow()
	if(self.config.init == nil) then
		if(mUI_VarGenBegin(self.config, "Minimap")) then
			mUI_VarGenBeginGroup(self.config, "Minimap", {0.63, 0.41, 0.27, .9})
				mUI_VarGenEnumeration(self.config, "Minimap_Zone_Pos", {"BOTTOM", "TOP"}, "Minimap Zone Position", "Sets the position of the zone frame")
				mUI_VarGenEnumeration(self.config, "Minimap_Icon_Side", {"LEFT", "RIGHT"}, "Minimap Icon/Button Side", "Arranges all minimap icons/buttons at the left or right side of the minimap")
				mUI_VarGenNumberField(self.config, "Minimap_Icon_Size", "Minimap Icon/Button Size")
				mUI_VarGenNumberField(self.config, "Minimap_Width", "Minimap Width")
				mUI_VarGenNumberField(self.config, "Minimap_Height", "Minimap Height")
			mUI_VarGenEndGroup(self.config)
			mUI_VarGenEnd(self.config)
		end
	else
		self.config:Show()
	end	
end

function minimap:OnGlobalUnlock()

	-- 3rd party questie workaround!
	if(Questie.db.global.disableAvailable ~= nil) then
		Questie.db.global.disableAvailable = true
		QuestieQuest:UpdateHiddenNotes();
	end
end

function minimap:OnGlobalLock()

	-- 3rd party questie workaround!
	if(Questie.db.global.disableAvailable ~= nil) then
		Questie.db.global.disableAvailable = false
		QuestieQuest:UpdateHiddenNotes();
	end
end
