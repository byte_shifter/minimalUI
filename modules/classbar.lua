local classbar = mUI:RegisterModule("Classbar")
classbar:RegisterEvents({"PLAYER_ENTERING_WORLD", "LEARNED_SPELL_IN_TAB", "SPELL_UPDATE_COOLDOWN", "UNIT_POWER_UPDATE"})


local PowerBarUpdateTable = {}
local function PowerBarUpdateGeneric(powerbar) 
	local powertype = UnitPowerType("player")
	if(powertype and powertype <= 3) then
		powerbar:SetStatusBarColor(mui_unpack_color(MUIPOWERCOLORS[powertype+1], .75))
	end
	
	powerbar:SetMinMaxValues(0, UnitPowerMax("player"))
	local power = UnitPower("player")
	powerbar:SetValue(power)
	powerbar.text:SetText(power)
end
PowerBarUpdateTable["Mage"]    = PowerBarUpdateGeneric
PowerBarUpdateTable["Warrior"] = PowerBarUpdateGeneric
PowerBarUpdateTable["Hunter"]  = PowerBarUpdateGeneric
PowerBarUpdateTable["Shaman"]  = PowerBarUpdateGeneric
PowerBarUpdateTable["Druid"]   = PowerBarUpdateGeneric
PowerBarUpdateTable["Priest"]  = PowerBarUpdateGeneric
PowerBarUpdateTable["Warlock"] = PowerBarUpdateGeneric
PowerBarUpdateTable["Rogue"]   = function (powerbar)
	PowerBarUpdateGeneric(powerbar)
	powerbar.text:SetText(powerbar.text:GetText() .. " [" .. GetComboPoints("player", "target") .. "]")
end

local function UpdateSpellbookSpellTable()
	if(not classbar.spells) then
		classbar.spells = {}
	end
	
	local i = 1
	while true do
		local spellName, spellSubName = GetSpellBookItemName(i, BOOKTYPE_SPELL)
		if not spellName then
			do break end
		end
	   
		if(classbar.spells[spellName] == nil) then
			classbar.spells[spellName] = {}
			classbar.spells[spellName].name = spellName
			classbar.spells[spellName].id   = i
		end
		i = i + 1
	end
end

local function CooldownBarSortCooldownsByDuration(self)
	sort(self.slots, function (a, b)
		if(a and b) then
			return a.endtime < b.endtime
		else
			return -1
		end
	end)
	
	for i=1, 16 do
		if(self.slots[i].spellname) then
			local w = self.slots[i]:GetWidth()
			self.slots[i]:SetPoint("CENTER", self.splitterleft, "CENTER", -(1 + (i * (w + 2))) + w * 0.5, 0)
			UIFrameFadeIn(self.slots[i], 0.5, 0.0, 1.0)
		end
	end
	
	--if(self.slots[1].spellname) then
--		UIFrameFadeIn(self.slots[1], 0.5, 0.0, 1.0)
--	end
end

function classbar:OnVarChange(name, value)
	if(name == "Cooldown_Tracker_Icon_Size") then
		if(self.cooldownbar.slots) then
			for i=1, 16 do
				self.cooldownbar.slots[i]:SetSize(value, value)
			end
			CooldownBarSortCooldownsByDuration(self.cooldownbar)
		end
	end
end

local function CooldownBarUpdateSpellbook()
	for i, spell in pairs(classbar.spells) do
		local starttime, duration, enabled = GetSpellCooldown(spell.name)
		if(duration and duration > 2.5) then
			classbar.cooldownbar:TrackCooldown(spell.name, starttime, duration)
		end
	end
end

function classbar:OnEvent(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12)
	if(event == "SPELL_UPDATE_COOLDOWN") then
		CooldownBarUpdateSpellbook()
		
		if(IsUsableSpell("Riposte"))then
		--	cprint("RIPOSTE magh duuudde!!!!")
		end	
	elseif(event == "UNIT_POWER_UPDATE") then
		if(arg1 == "player") then
			PowerBarUpdateTable[self.class](self.powerbar)
		end
	elseif(event == "PLAYER_ENTERING_WORLD" or event == "LEARNED_SPELL_IN_TAB") then
		UpdateSpellbookSpellTable()
		CooldownBarUpdateSpellbook()
		PowerBarUpdateGeneric(self.powerbar)
	end
end


local function CooldownBarTrackCooldown(self, spellname, starttime, duration)
	for i=1, 16 do
		if(self.slots[i].spellname == spellname) then 
			self.slots[i].starttime = starttime
			self.slots[i].endtime = self.slots[i].starttime + duration
			return 
		end
	end

	for i=1, 16 do
		
		
		if(self.slots[i].spellname == nil) then
			self.slots[i].spellname = spellname
			self.slots[i].starttime = starttime
			self.slots[i].endtime   = self.slots[i].starttime + duration
			
			local name, rank, icon, casttime, minrange, maxrange = GetSpellInfo(spellname)
			self.slots[i].tex:SetTexture(icon)
			self.slots[i].tex:SetTexCoord(.075, .925, .075, .925)
			self.slots[i]:SetFrameLevel(1)
			self.slots[i].do_anim = nil
			self.slots[i].t       = 1.0
			
			self.slots[i]:Show()
			CooldownBarSortCooldownsByDuration(self)
			return
		end
	end
	
	
end

function classbar:OnEnable()
	self.class = UnitClass("player")
	
	
	self.cooldownbar = uiCreateElement(UIParent, "Frame", "Classbar", 120, 32)
	self.cooldownbar:RegisterGlobalDragging()
	self.cooldownbar:EnableMouse(false)
	self.cooldownbar:SetBackgroundColor(0,0,0,0)
	self.cooldownbar:SetBorderColor(0,0,0,0)
	self.cooldownbar:SetShadowColor(0,0,0,0)
	self.cooldownbar:SetPoint("CENTER", UIParent, "CENTER")
		
	local splitleft = self.cooldownbar:CreateTexture("SplitterLeft", "ARTWORK")
	splitleft:SetColorTexture(1,1,1,1)
	splitleft:SetVertexColor(0,0,0,.9)
	splitleft:SetSize(1, 32)
	splitleft:SetPoint("LEFT", self.cooldownbar, "CENTER", -21, 0)
	self.cooldownbar.splitterleft = splitleft
	
	local splitright = self.cooldownbar:CreateTexture("SplitterRight", "ARTWORK")
	splitright:SetColorTexture(1,1,1,1)
	splitright:SetVertexColor(0,0,0,.9)
	splitright:SetSize(1, 32)
	splitright:SetPoint("LEFT", self.cooldownbar, "CENTER", 20, 0)
	self.cooldownbar.splitright = splitright

	local powerbar = CreateFrame("Statusbar", self.cooldownbar:GetName() .. "PowerBar", self.cooldownbar)
	powerbar:SetPoint("TOPLEFT", splitleft, "TOPRIGHT")
	powerbar:SetPoint("BOTTOMRIGHT", splitright, "BOTTOMLEFT")
	powerbar:SetOrientation("VERTICAL")
	powerbar:SetStatusBarTexture(MUI_TEXTURES[2])
	powerbar.text = uiFontString(powerbar, "Text")
	PowerBarUpdateTable[self.class](powerbar)
	self.powerbar = powerbar
	

	self.cooldownbar.TrackCooldown = CooldownBarTrackCooldown
	self.cooldownbar.slots = {}
	self.cooldownbar.count = 0
	for i = 1, 16 do
		local spellicon = uiCreateElement(self.cooldownbar, "Frame", "SpellIcon"..i, 26, 26)		
		spellicon:ClearAllPoints()
		spellicon:SetBorderColor(0, 0, 0, 1)
		spellicon:SetShadowColor(0,0,0,1)
		spellicon.text = uiFontString(spellicon, "")
		spellicon.text:SetFontObject(MUIDEFAULTFONT)	
		spellicon.endtime = 99999999
		
		local tex = spellicon:CreateTexture(spellicon:GetName().."Texture", "ARTWORK")
		tex:SetColorTexture(1,1,1,1)
		tex:SetAllPoints(spellicon)	
		spellicon.tex = tex
		
		spellicon:Hide()
		spellicon:SetScript("OnUpdate", function (this, dt)
		
			if(this.do_anim) then
				local scale = mui_min(this:GetScale() * 1.1, 2.0)
				this:SetScale(scale)
				local c = this.endtime - GetTime()
				if(c > 0.0) then
					this.text:SetText(string.format("%.1f", c))
				end
			else
				this:SetScale(1.0)
			end
		
			if(this.t > 1.0) then
				local curtime = GetTime()
				local duration = ceil(this.endtime - curtime)
				if(duration <= 0) then
					this.spellname = nil
					this:Hide()
					this.endtime = 99999999
					this.do_anim = nil
					CooldownBarSortCooldownsByDuration(this:GetParent())
				else				
					if(duration <= 60) then
						this.text:SetText(duration)
						if(duration == 1) then
							UIFrameFadeOut(this, 1.0, 1.0, 0.0)
							this.do_anim = true
							this:SetFrameLevel(8)
--							local p, par, parp, x, y = this:GetPoint(1)
--							this:SetPoint("CENTER", x, this:GetWidth())
						end
					else
						this.text:SetText(mui_abbrev_seconds(duration))
					end
				end
				this.t = 0
			end
			this.t = this.t + dt
		end)
		self.cooldownbar.slots[i] = spellicon
	end
end

function classbar:OnConfigShow()
	if(self.config.init == nil) then
		if(mUI_VarGenBegin(self.config, self.name)) then
			mUI_VarGenBeginGroup(self.config, "Cooldown Tracker")
				mUI_VarGenNumberField(self.config, "Cooldown_Tracker_Icon_Size", "Cooldown Tracker Icon Size")
			mUI_VarGenEndGroup(self.config)
			mUI_VarGenEnd(self.config)
		end
	else
		self.config:Show()
	end	
end

function classbar:OnGlobalUnlock()
	self.cooldownbar:EnableMouse(true)
end

function classbar:OnGlobalLock()
	self.cooldownbar:EnableMouse(false)
end