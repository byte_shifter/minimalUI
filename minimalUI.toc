## Interface: 11302

## Title: |cFFB22222minimal|cFFFFFFFFUI
## Notes: minimalUI
## Author: Andreas Gaida
## Version: 0.1.5
## DefaultState: enabled
## LoadOnDemand: 0
## SavedVariables: mUI_GVARS
## SavedVariablesPerCharacter: mUI_CVARS

presets.lua

api\core.lua
api\tables.lua
api\wrapper.lua

ui\core.lua
ui\utils.lua
ui\popup.lua

api\vars.lua

minimalUI.lua

modules\system.lua
modules\bags.lua
modules\minimap.lua
modules\classbar.lua
