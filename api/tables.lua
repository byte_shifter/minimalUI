MUIDEFAULTFONT = CreateFont("mUIDefaultFont")
MUIDEFAULTFONT:SetFont("Interface\\AddOns\\minimalUI\\fonts\\big_noodle_titling.ttf", 14, "OUTLINE")

MUICOLORS = {
	{ mui_rgba_byte_to_float(252,  92, 101, 255) },
	{ mui_rgba_byte_to_float(253, 150,  68, 255) },
	{ mui_rgba_byte_to_float(254, 211,  48, 255) },
	{ mui_rgba_byte_to_float( 38, 222, 129, 255) },
	{ mui_rgba_byte_to_float( 43, 203, 186, 255) },
	{ mui_rgba_byte_to_float( 69, 170, 242, 255) },
	{ mui_rgba_byte_to_float( 75, 123, 236, 255) },
	{ mui_rgba_byte_to_float(165,  94, 234, 255) },
	{ mui_rgba_byte_to_float(209, 216, 224, 255) },
	{ mui_rgba_byte_to_float(119, 140, 163, 255) },
}

MUIPOWERCOLORS = {
   	{ 0.0,  0.4,  1.0,  .9 }, -- MANA
   	{ 0.8,  0.2,  0.2,  .9 }, -- RAGE
   	{ 1, 	0.5,  0.25, .9 }, -- FOCUS
   	{ 1, 	0.96, 0.41, .9 }  -- ENERGY
}

MUI_CLASSES = {
	["WARRIOR"] = { r = 0.78, g = 0.61, b = 0.43, a = 1 },
	["MAGE"] 	= { r = 0.41, g = 0.8,  b = 0.94, a = 1 },
	["ROGUE"] 	= { r = 1, 	  g = 0.96, b = 0.41, a = 1 },
	["DRUID"] 	= { r = 1,    g = 0.49, b = 0.04, a = 1 },
	["HUNTER"] 	= { r = 0.67, g = 0.83, b = 0.45, a = 1 },
	["SHAMAN"] 	= { r = 0.14, g = 0.35, b = 1.0,  a = 1 },
	["PRIEST"] 	= { r = 0.85, g = 0.85, b = 0.85, a = 1 },
	["WARLOCK"] = { r = 0.58, g = 0.51, b = 0.79, a = 1 },
	["PALADIN"] = { r = 0.96, g = 0.55, b = 0.73, a = 1 }	
}



MUI_FONTS = {
	"Interface\\AddOns\\minimalUI\\fonts\\8_Bit_Madness.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Alterebro.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Asai-Analogue.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Big_Noodle_Titling.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\capture.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Homespun.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\macromedia.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Pixelade.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Pixelbold.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Pixeltype.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Pricedown bl.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Silkscreen.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Uni.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Upheavtt.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Visitor1.ttf",
	"Interface\\AddOns\\minimalUI\\fonts\\Visitor2.ttf",
	"Fonts\\FRIZQT__.TTF",
	"Fonts\\ARIALN.ttf",
	"Fonts\\skurri.ttf",
	"Fonts\\Morpheus.TTF",
}

MUI_TEXTURES = {
	"Interface\\AddOns\\minimalUI\\img\\Aluminium.tga",
	"Interface\\AddOns\\minimalUI\\img\\BackdropSolid.tga",
	"Interface\\AddOns\\minimalUI\\img\\BantoBar.tga",
	"Interface\\AddOns\\minimalUI\\img\\Bars.tga",
	"Interface\\AddOns\\minimalUI\\img\\Gloss.tga",
	"Interface\\AddOns\\minimalUI\\img\\Healbot.tga",
	"Interface\\AddOns\\minimalUI\\img\\Luna.tga",
	"Interface\\AddOns\\minimalUI\\img\\Otravi.tga",
	"Interface\\AddOns\\minimalUI\\img\\Smooth.tga",
	"Interface\\AddOns\\minimalUI\\img\\Striped.tga"
}