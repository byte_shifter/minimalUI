-- minimalUI World of Warcraft Lua Wrapper for 1.12.1
function wapiGetResolution()
	local resolution = GetCVar("gxWindowedResolution")
    local res = { strsplit("x", resolution) }
	return tonumber(res[1]), tonumber(res[2])
end

--[[
==========================================================================================================

1.12.1

WoW Hoverbind Manager

]]--

local function wSaveBindings(bindings)
	if(InCombatLockdown()) then
		cerror("Can't set bindings while in combat!")
		return
	else
		if not SaveBindings then
			AttemptToSaveBindings(bindings)
		else
			SaveBindings(binding)
		end
	end	
end

local function wSetBinding(key, button, alt_button)
	if(InCombatLockdown()) then
		cerror("Can't save bindings while in combat!")
		return
	else
		SetBinding(key, button, alt_button)
	end	
end

UI_BINDING_MANAGER = nil
local function uiHoverbindGetHoverButton()
	if(GetMouseFocus()) then
		local name = GetMouseFocus():GetName()
		if(name) then
			
			local match, _, buttonname, index = strfind(name, "^(%a+)(%d+)$")
			
			if(buttonname == "MultiBarBottomLeftButton") then
				return "MULTIACTIONBAR1BUTTON"..index, name
			elseif(buttonname == "MultiBarBottomRightButton") then
				return "MULTIACTIONBAR2BUTTON"..index, name
			elseif(buttonname == "MultiBarLeftButton") then
				return "MULTIACTIONBAR4BUTTON"..index, name
			elseif(buttonname == "MultiBarRightButton") then
				return "MULTIACTIONBAR3BUTTON"..index, name
			elseif(buttonname == "BonusActionButton") then
				return "ACTIONBUTTON"..index, name
			elseif(buttonname == "ShapeshiftButton") then
				return "SHAPESHIFTBUTTON"..index, name
			elseif(buttonname == "PetActionButton") then
				return "BONUSACTIONBUTTON"..index, name
			elseif(buttonname == "ActionButton") then
				return "ACTIONBUTTON"..index, name
			elseif(buttonname == "StanceButton") then
				return "SHAPESHIFTBUTTON"..index, name
			end
		end
	end
	return nil
end

local function uiHoverbindOnKeyUpHandler(self, arg1)
	local key = ""
	if(IsShiftKeyDown()) then key = key .. "SHIFT-" end
	if(IsAltKeyDown()) then key = key .. "ALT-" end
	if(IsControlKeyDown()) then key = key .. "CTRL-" end
	key = key .. arg1

	local button, uibtn = uiHoverbindGetHoverButton()
	if(button) then
		if(arg1 == "ESCAPE")  then
			key = GetBindingKey(button)
			if(key) then
				wSetBinding(key)
				wSaveBindings(2)

				-- @WORKAROUND Vanilla WoW still shows the hotkey even tho we remove its binding.
				--local hotkey = get_global(uibtn .."HotKey")
				--if(hotkey) then hotkey:SetText("") end
			end
		else
			wSetBinding(key, button)
			wSaveBindings(2)
		end
	else
		if(arg1 == "ESCAPE")  then
			self:Hide()
		end
	end
end
function uiHoverbindToggle()
	if(UI_BINDING_MANAGER) then
		if(not UI_BINDING_MANAGER:IsVisible()) then
			UI_BINDING_MANAGER:Show()
		else
			UI_BINDING_MANAGER:Hide()
		end
	end
end
function uiHoverbindInitialize()
	if(UI_BINDING_MANAGER == nil) then
		LoadBindings(2)
	
		UI_BINDING_MANAGER = uiCreateElement(UIParent, "Frame", "BindingManager")
		UI_BINDING_MANAGER:SetAllPoints(UIParent)
		UI_BINDING_MANAGER:SetFrameStrata(BACKGROUND)
		UI_BINDING_MANAGER:SetBackgroundColor(0.02, 0.02, 0.02, .85)
		UI_BINDING_MANAGER:EnableKeyboard(1)
		UI_BINDING_MANAGER:EnableMouse(1)
		UI_BINDING_MANAGER:SetScript("OnKeyUp", uiHoverbindOnKeyUpHandler)
		UI_BINDING_MANAGER:Hide() -- Disabled by default

		local title = uiFontString(UI_BINDING_MANAGER, "Press |cffffff22ESCAPE|r (while not hovering over an actionbutton) to quit hoverbind mode!", nil, nil, nil, nil, "TOP", "CENTER", 1)
		title:ClearAllPoints()
		title:SetPoint("TOP", UI_BINDING_MANAGER, "TOP", 0, -32)
--		uiFrameSetGameFont(title, "GameFontNormal")

		UI_BINDING_MANAGER.MOUSE_BTN_STATES = {}
		local function uiBindingManagerUpdateMouseState()
			UI_BINDING_MANAGER.MOUSE_BTN_STATES["Button1"] = IsMouseButtonDown("Button1")
			UI_BINDING_MANAGER.MOUSE_BTN_STATES["Button2"] = IsMouseButtonDown("Button1")
			UI_BINDING_MANAGER.MOUSE_BTN_STATES["Button3"] = IsMouseButtonDown("Button3")
			UI_BINDING_MANAGER.MOUSE_BTN_STATES["Button4"] = IsMouseButtonDown("Button4")
			UI_BINDING_MANAGER.MOUSE_BTN_STATES["Button5"] = IsMouseButtonDown("Button5")
		end
		local function uiBindingManagerGetPressedMouseBtn()
			for key, state in pairs(UI_BINDING_MANAGER.MOUSE_BTN_STATES) do
				if(state and not IsMouseButtonDown(key)) then
					return key:upper()
				end
			end
			return nil
		end
		
		uiBindingManagerUpdateMouseState()
		uiBindingManagerGetPressedMouseBtn()
		
		UI_BINDING_MANAGER.info = uiFontString(UI_BINDING_MANAGER, "Currently hovering: None", nil, nil, nil, nil, "TOP", "CENTER", 1)
		UI_BINDING_MANAGER.info:ClearAllPoints()
		UI_BINDING_MANAGER.info:SetPoint("TOP", title, "TOP", 0, -32)
--		uiFrameSetGameFont(UI_BINDING_MANAGER.info, "GameFontNormalHuge")
		UI_BINDING_MANAGER:SetScript("OnUpdate", function (self, delta)
			-- body
			local button, uibtn = uiHoverbindGetHoverButton()
			if(button) then
				local binding, alt_binding = GetBindingKey(button)
				local clear_text = "\nPress |cffffff22ESCAPE|r to remove current binding!"
				if binding then 
					if alt_binding then binding = binding .. " or " .. alt_binding end
				else
					clear_text = "" 
				end
				
				self.info:SetText("Currently hovering: |cffffff22" .. (uibtn or "None") .. "|r [|cffff22ff" .. (binding or "None") .. "|r]" .. clear_text)
				
				local mouse_btn_pressed = uiBindingManagerGetPressedMouseBtn()
				if(mouse_btn_pressed) then
					local key = ""
					if(IsShiftKeyDown()) then key = key .. "SHIFT-" end
					if(IsAltKeyDown()) then key = key .. "ALT-" end
					if(IsControlKeyDown()) then key = key .. "CTRL-" end
					key = key .. mouse_btn_pressed
										
					wSetBinding(key, button)
					wSaveBindings(2)
				end
			else
				self.info:SetText("Currently hovering: None")
			end
			
			uiBindingManagerUpdateMouseState()
		end)
		
	end
end


--[[
==========================================================================================================

1.12.1

WoW UI Inventory Helper

]]--

ITEM_QUALITY_POOR 		= 0
ITEM_QUALITY_COMMON 	= 1
ITEM_QUALITY_UNCOMMON 	= 2
ITEM_QUALITY_RARE 		= 3
ITEM_QUALITY_EPIC 		= 4
ITEM_QUALITY_LEGENDARY	= 5

-- @param arg1 - argN | bag indicies
function uiGetBagHandler(bag1, bag2, bag3, bag4, bag5, bag6, bag7, bag8)
	local bags 			= {}

	local bag_update = function(self)
		self.name 		= nil
		self.slots 		= 0
		self.slots_free = 0
		--self.type 		= GetBagFamily(self.id)
		
		self.name 	= GetBagName(self.id)
		self.slots 	= GetContainerNumSlots(self.id)
		
		for slot_index=1, self.slots do
			local itemlink 	= GetContainerItemLink(self.id, slot_index)
			local item 		= self.items[slot_index]
			if(not item) then item = {} end
			
			if(itemlink) then
				local _, _, linkstr = string.find(itemlink, "(item:%d+:%d+:%d+:%d+)")
				item.name, _, item.quality, item.level, item.level_min, item.type, item.subtype = GetItemInfo(itemlink)
				local mm = { GetItemInfo(itemlink) }
				--cdebugx(mm)
				--item = { GetItemInfo(itemlink) }
			else
				item.name 		= nil
				item.quality 	= nil
				self.slots_free = self.slots_free + 1
			end
			item.texture, item.count, item.locked = GetContainerItemInfo(self.id, slot_index)

			self.items[slot_index] = item
		end		
	end
	
	
	for index, bagid in ipairs({bag1, bag2, bag3, bag4, bag5, bag6, bag7, bag8}) do 
		local bag 		= {}
		bag.items 		= {}
		bag.id 			= bagid
		bag.Update 		= bag_update
		bag:Update()
		table.insert(bags, bag)
	end

	bags.GetSlotCount = function(self)
		local num_slots 	 = 0
		local num_slots_free = 0
		for index, bag in ipairs(bags) do
			if(bag.id ~= -2) then
				num_slots 		= num_slots + bag.slots
				num_slots_free 	= num_slots_free + bag.slots_free
			end
		end	
		return num_slots, num_slots_free
	end
	return bags	
end

--[[
==========================================================================================================

1.13.12

WoW Profile Manager

]]--

-- @return String representation of the current profile
function wapiGetCurrentProfileName()
	return UnitName("player").."@"..GetRealmName()
end

-- @return Table of profiles which match the specified module
-- @return Number of profiles that match the specified module
function wapiGetProfilesForModule(module)
	local current_profile = wapiGetCurrentProfileName()
	local num_profiles = 0
	local profiles = {}
	for name, value in pairs(mUI_GVARS["Profiles"]) do
		if(mUI_GVARS["Profiles"][name][module]) then
			if(name ~= current_profile) then
				table.insert(profiles, name)
				num_profiles = num_profiles + 1
			end
		end
	end
	return profiles, num_profiles
end

-- @return profile table of the specified name, nil if it does not exist
function wapiGetProfile(name)
	local profile = mUI_GVARS["Profiles"][name]
	if(profile) then
		return profile
	end
end

-- @return current profile table
function wapiGetCurrentProfile()
	return mUI_CVARS
end

-- Stores current player profile to the global settings
function wapiSaveCurrentProfile()
	if(mUI_GVARS["Profiles"] == nil) then 
		mUI_GVARS["Profiles"] = {}
	end
	local profile = wapiGetCurrentProfileName()
	mUI_GVARS["Profiles"][profile] = mUI_CVARS
end

--[[
==========================================================================================================

1.12.1

WoW UI Elements Utility

]]--

UI_DEFAULT_BG_COLOR 		= {0.020,0.020,0.020,0.950}
UI_DEFAULT_FG_COLOR1 		= {0.060,0.060,0.060,0.950}
UI_DEFAULT_FG_COLOR2 		= {0.200,0.200,0.200,0.900}
UI_DEFAULT_FG_COLOR3 		= {0.120,0.120,0.120,1.000}
UI_DEFAULT_HIGHLIGHT		= {1.000,1.000,1.000,0.150}
UI_DEFAULT_TX_COLOR1		= {1.000,1.000,1.000,1.000}

function uiAddThinBorder(frame, packed_color)
	if not frame then return end

	if(frame.border == nil) then
		frame.border = {}
		for i=1, 4 do
			frame.border[i] = frame:CreateTexture(frame:GetName() .. "Border"..i, "OVERLAY")
			frame.border[i]:SetColorTexture(1,1,1,1)
		end
		
		frame.border[1]:SetPoint("TOPLEFT", frame, "TOPLEFT", 0.0, 0.0)
		frame.border[1]:SetPoint("BOTTOMRIGHT", frame, "TOPRIGHT", 0.0, -1.0)
		frame.border[2]:SetPoint("TOPLEFT", frame, "TOPLEFT", 0.0, -1.0)
		frame.border[2]:SetPoint("BOTTOMRIGHT", frame, "BOTTOMLEFT", 1.0, 1.0)
		frame.border[3]:SetPoint("TOPLEFT", frame, "BOTTOMLEFT", 0.0, 1.0)
		frame.border[3]:SetPoint("BOTTOMRIGHT", frame, "BOTTOMRIGHT", 0.0, 0.0)
		frame.border[4]:SetPoint("TOPLEFT", frame, "TOPRIGHT", -1.0, -1.0)
		frame.border[4]:SetPoint("BOTTOMRIGHT", frame, "BOTTOMRIGHT", 0.0, 1.0)
	end

	frame.SetBorderColor = function (self, r, g, b, a)
		local color = {}
		if(r and g and b and a) then
			color[1] = r color[2] = g color[3] = b color[4] = a
		else
			color = UI_DEFAULT_FG_COLOR1
		end

		self.border[1]:SetVertexColor(unpack(color))
		self.border[2]:SetVertexColor(unpack(color))
		self.border[3]:SetVertexColor(unpack(color))
		self.border[4]:SetVertexColor(unpack(color))	
	end
	
	frame.ShowBorder = function (self, show)
		if(show) then
			self.border[1]:Show()
			self.border[2]:Show()
			self.border[3]:Show()
			self.border[4]:Show()
		else
			self.border[1]:Hide()
			self.border[2]:Hide()
			self.border[3]:Hide()
			self.border[4]:Hide()		
		end
	end

	if(packed_color) then
		frame:SetBorderColor(unpack(packed_color))
	else
		frame:SetBorderColor(0.0, 0.0, 0.0, 1.0)
	end
end

function uiAddWindowBorder(frame, color)
	if not frame then return end

	if(frame.border == nil) then
		frame.border = {}
		for i=1, 4 do
			frame.border[i] = frame:CreateTexture(nil, "BORDER")
			frame.border[i]:SetColorTexture(1,1,1,1)
		end
		
		frame.border[1]:SetPoint("TOPLEFT", frame, "TOPLEFT", 0.0, 0.0)
		frame.border[1]:SetPoint("BOTTOMRIGHT", frame, "TOPRIGHT", 0.0, -18.0)
		frame.border[2]:SetPoint("TOPLEFT", align_to or frame, "TOPLEFT", 0.0, -1.0)
		frame.border[2]:SetPoint("BOTTOMRIGHT", align_to or frame, "BOTTOMLEFT", 1.0, 1.0)
		frame.border[3]:SetPoint("TOPLEFT", align_to or frame, "BOTTOMLEFT", 0.0, 1.0)
		frame.border[3]:SetPoint("BOTTOMRIGHT", align_to or frame, "BOTTOMRIGHT", 0.0, 0.0)
		frame.border[4]:SetPoint("TOPLEFT", align_to or frame, "TOPRIGHT", -1.0, -1.0)
		frame.border[4]:SetPoint("BOTTOMRIGHT", align_to or frame, "BOTTOMRIGHT", 0.0, 1.0)
	end

	frame.SetBorderColor = function (self, r, g, b, a)
		local color = {}
		if(r and g and b and a) then
			color[1] = r color[2] = g color[3] = b color[4] = a
		else
			color = UI_DEFAULT_FG_COLOR1
		end

		self.border[1]:SetVertexColor(unpack(color))
		self.border[2]:SetVertexColor(unpack(color))
		self.border[3]:SetVertexColor(unpack(color))
		self.border[4]:SetVertexColor(unpack(color))	
	end
	
	frame.ShowBorder = function (self, show)
		if(show) then
			self.border[1]:Show()
			self.border[2]:Show()
			self.border[3]:Show()
			self.border[4]:Show()
		else
			self.border[1]:Hide()
			self.border[2]:Hide()
			self.border[3]:Hide()
			self.border[4]:Hide()		
		end
	end

	frame:SetBorderColor(r, g, b, a)
end

-- TODO(aga) Make the cooldown thingy a seperate frame so we dont have to hook the OnUpdate function!
COOLDOWN_CONTAINER 	= 1
COOLDOWN_INVENTORY	= 2
COOLDOWN_ACTION		= 3
COOLDOWN_ITEM		= 4
COOLDOWN_SPELL		= 5

local function uiCooldownUpdateHandler(this, arg1)
	this.dt = this.dt + arg1
	if(this.dt >= 1.0) then
		local current_time = GetTime()
		if(this.start > 0 and current_time <= this.finish) then
			this.overlay.text:SetText(mui_abbrev_seconds(this.finish - current_time))
			if(current_time >= this.finish) then
				this.start  = 0
				this.finish = 0
			end
		else
			this.overlay.text:SetText("")
			this:Hide()
		end
		this.dt = 0
	end
end

function uiCooldownUpdate(frame)
	if(frame.cooldown.ctype == COOLDOWN_CONTAINER) then
		frame.cooldown.start, duration, enabled = GetContainerItemCooldown(frame.cooldown.bagid, frame.cooldown.slotid)
		if(not enabled) then
			frame.cooldown.start  = 0
			frame.cooldown.finish = 0
		else
			if(duration and duration > 0) then
				frame.cooldown.finish = frame.cooldown.start + duration
				if(not frame.cooldown:IsVisible()) then
					frame.cooldown.dt = 1.0
					frame.cooldown:Show()
				end
			else
				frame.cooldown:Hide()
			end
		end
	end
end

--[[
	Valid cooldown types are:
 		1: ContainerItemCooldown
]]--
function uiAddCooldownFrame(frame, cooldown_type, arg1, arg2, arg3, arg4, arg5)
	if(frame.cooldown == nil and cooldown_type ~= nil) then
		frame.cooldown = CreateFrame("Frame", frame:GetName() .. "CooldownFrame", frame)
		frame.cooldown.start 	= 0
		frame.cooldown.finish 	= 0
		if(cooldown_type == COOLDOWN_CONTAINER) then
			frame.cooldown.ctype   = COOLDOWN_CONTAINER
			frame.cooldown.bagid  = arg1
			frame.cooldown.slotid = arg2
		end
		
		frame.cooldown:SetAllPoints(frame)
		
		frame.cooldown.overlay = frame.cooldown:CreateTexture(nil, "OVERLAY")
		frame.cooldown.overlay:SetPoint("TOPLEFT", frame.cooldown, "TOPLEFT", 1, -1)
		frame.cooldown.overlay:SetPoint("BOTTOMRIGHT", frame.cooldown, "BOTTOMRIGHT", -1, 1)
		frame.cooldown.overlay:SetTexture(1,1,1,1)
		frame.cooldown.overlay:SetVertexColor(0,0,0,.75)
	
		frame.cooldown.overlay.text = uiFontString(frame.cooldown, "", nil, nil, nil, "OVERLAY", true, true)
		frame.cooldown.overlay.text:SetAllPoints(frame.cooldown)
		frame.cooldown:Hide()
		frame.cooldown:SetScript("OnUpdate", uiCooldownUpdateHandler)
	end
end
