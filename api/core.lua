-- Global enumaration type variables
PRIORITY_LOW 		= 0
PRIORITY_MEDIUM 	= 1
PRIORITY_HIGH 		= 2
PRIORITY_SYSTEM 	= 3


MUI_CORE_FONT 		= "Interface\\AddOns\\minimalUI\\fonts\\alterebro.ttf"
MUI_CORE_FONTSIZE 	= 13
--if(GetCVar("UseUIScale") == "1") then
--	MUI_CORE_FONTSIZE = 16
--end

MUI_SOLID_BACKDROP = { 
	bgFile = "Interface\\AddOns\\minimalUI\\img\\BackdropSolid.tga", 
	edgeFile = "", 
	tile = false, tileSize = 0, edgeSize = 8, 
	insets = { left = 0, right = 0, top = 0, bottom = 0 }
}

MUI_SOLID_BACKDROP_BORDER = { 
	bgFile = "Interface\\AddOns\\minimalUI\\img\\BackdropSolid.tga", 
	edgeFile = "Interface\\AddOns\\minimalUI\\img\\BorderGlow.tga",  
	tile = false, tileSize = 0, edgeSize = 4, 
	insets = { left = 0, right = 0, top = 0, bottom = 0 }
}


-- Error and Debugging helpers
function cerror(message, debugstack_depth)
	local debugmessage = debugstack(debugstack_depth or 2,1,1) or ""
	DEFAULT_CHAT_FRAME:AddMessage("|cffff5555Error: " .. tostring(message) .."\n"..debugmessage)
end

function cdebug(message)
	DEFAULT_CHAT_FRAME:AddMessage("|cffffff22DEBUG:|r |cffffff88" .. tostring(message))
end

function cdebugx(list)
	local t = ""
	for i, v in pairs(list) do
		t = t .. tostring(v) .. ";"
	end
	cdebug(t)
end

function cprint(message)
	DEFAULT_CHAT_FRAME:AddMessage("|cffa11f1fminimalUI|r> "..tostring(message))
end


function iterate_children(frame)
	if(frame and type(frame) == "table" and frame.GetObjectType) then
		cdebugx({frame:GetName(), frame:GetObjectType()})
		local children = { frame:GetChildren() }
		for i, child in pairs(children) do
			iterate_children(child)
		end
	end
end

function iterate_regions(frame)
	if(frame and type(frame) == "table" and frame.GetRegions) then
		cdebugx({frame:GetName(), frame:GetNumRegions()})
		local children = { frame:GetRegions() }
		for i, child in pairs(children) do
			iterate_regions(child)
		end
	else
		if(frame.GetName) then
			if(frame:GetObjectType() == "Texture") then
				cdebugx({"> Texture", (frame:GetName() or "n/a"), frame:GetTexture(), frame:GetTexCoord()})
				--cdebugx({"--> ", frame:GetPoint(1)})
				--cdebugx({"--> ", frame:GetPoint(2)})
				--cdebugx({"--> ", frame:GetPoint(3)})
			else
				cdebugx({" > "..frame:GetObjectType(), frame:GetName() or "n/a"})
			end
		end
	end
end


-- Disables the ScriptErrors frame and redirects the output to the console
--ScriptErrors:SetScript("OnShow", function(msg)
--	local error_message = ScriptErrors_Message:GetText()
--	cerror(error_message)
--	ScriptErrors:Hide()
--end)



-- Table helpers

function mui_tcontains(table, item)
	local index = 1;
	while table[index] do
    	if(item == table[index]) then
    		return index;
    	end
    	index = index + 1;
	end
	return nil;
end

function mui_spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[getn(keys)+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

function mui_pairsByKeys (t, f)
    local a = {}
    for n in pairs(t) do table.insert(a, n) end
    table.sort(a, f)
    local i = 0      -- iterator variable
    local iter = function ()   -- iterator function
      i = i + 1
      if a[i] == nil then return nil
      else return a[i], t[a[i]]
      end
    end
    return iter
end


function mui_rgba_byte_to_float(r, g, b, a)
	return (r/255.0), (g/255.0), (b/255.0), (a/255.0)
end

function mui_max(a, b)
	if(a > b) then 
		return a 
	else
		return b
	end
end

function mui_min(a, b)
	if(a < b) then 
		return a 
	else
		return b
	end
end

function mui_range(value, min, max)
	if(value < min) then value = min 
	elseif(value > max) then value = max
	end
	return value
end

-- String helpers

function mui_abbrev_seconds(seconds)
	local tempTime;
	if ( seconds > 86400  ) then
		tempTime = ceil(seconds / 86400);
		return format("%sd", tempTime);
	end
	if ( seconds > 3600  ) then
		tempTime = ceil(seconds / 3600);
		return format("%sh", tempTime);
	end
	if ( seconds > 60  ) then
		tempTime = ceil(seconds / 60);
		return format("%sm", tempTime);
	end
	return format("%ss", ceil(seconds));
end

function mui_strsplit(delimiter, message, times)
	local wordsTable = {};
	if(times == null) then
		message = tostring(message) .. delimiter;
		while(string.find(message, delimiter)) do
			table.insert(wordsTable, string.sub(message, 0, string.find(message, delimiter)-1));
			message = string.sub(message, string.find(message, delimiter)+1, -1);
		end
		return unpack(wordsTable);
	end
	
	for i=1, times do
		if(i == times) then
			table.insert(wordsTable, message);
		else
			table.insert(wordsTable, string.sub(message, 0, string.find(message, delimiter)-1));
			message = string.sub(message, string.find(message, delimiter)+1, -1);
		end
	end
	return unpack(wordsTable);
end

function mui_strhas(str, list)
	for i, v in ipairs(list) do
		if(str == v) then return true end
	end
	return false
end

-- Color helpers
function mui_hexi(value)
	local hex = ""
	while(value > 0) do
		local index = mod(value, 16) + 1
		value = math.floor(value / 16)
		hex = string.sub('0123456789ABCDEF', index, index) .. hex
	end
	if(string.len(hex) == 0) then hex = '00' 
	elseif(string.len(hex) == 1) then hex = '0' .. hex
	end
	return hex
end

function mui_hexf(value)
	local v = value	
	return mui_hexi(v * 255)
end

function mui_colortohexstr(color)
	local hex = "#"
	hex = hex .. mui_hexf(color[4])
	hex = hex .. mui_hexf(color[1])
	hex = hex .. mui_hexf(color[2])
	hex = hex .. mui_hexf(color[3])
	return hex
end

function mui_color2chat(color)
	local hex = "|c"
	if(color[4]) then
		hex = hex .. mui_hexf(color[4])
	else
		hex = hex .. "ff"
	end
	hex = hex .. mui_hexf(color[1])
	hex = hex .. mui_hexf(color[2])
	hex = hex .. mui_hexf(color[3])
	return hex
end

function mui_hexstrtocolor(hexstring) -- Expecting #AARRGGBB as the string format
	if(strlen(hexstring) ~= 9) then 
		cerror("Invalid hexstring : "..hexstring..". Expecting #AARRGGBB as the string format.") 
		return nil
	end

	local red   = tonumber(strsub(hexstring, 4, 5), 16)
	local green = tonumber(strsub(hexstring, 6, 7), 16)
	local blue  = tonumber(strsub(hexstring, 8, 9), 16)
	local alpha = tonumber(strsub(hexstring, 2, 3), 16)
	if(red and green and blue and alpha) then
		return { red / 255.0, green / 255.0, blue / 255.0, alpha / 255.0 }
	else
		cerror("Invalid hexstring : "..hexstring..". Only values between #00000000 - #FFFFFFFF are allowed!") 
		return nil
	end
end

function mui_unpack_color(color, alpha)
	return color[1], color[2], color[3], alpha or color[4]
end

local _G = getfenv(0)
function get_global(v)
	if(v == nil) then return v end
	return _G[v]
end

