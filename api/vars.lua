MUI_CONFIG_FRAME 		= nil
MUI_VARS_TABLE 			= {}

local function mUI_VarColorFromValue(value)
	return {value.r, value.g, value.b, value.a}
end

local function mUI_VarValueFromColor(color)
	return { r = color[1], g = color[2], b = color[3], a = color[4] }
end

local function mUI_VarsGlobalInit(version)
	mUI_GVARS = {}
	mUI_GVARS["Version"] = version
	mUI_GVARS["Profiles"] = {}

	if(MUI_DEFAULT_PRESET) then
		mUI_GVARS["Profiles"]["minimalUI"] = MUI_DEFAULT_PRESET
	end
end

local function mUI_VarsCharacterInit(version)
	mUI_CVARS = {}

	if(MUI_DEFAULT_PRESET) then
		mUI_CVARS = MUI_DEFAULT_PRESET
		cprint("No preset found. Loading preset ["..mui_color2chat(MUICOLORS[1]).."minimalUI|r]")
	end
end

function mUI_VarsInitialize(version)
	if(mUI_GVARS == nil) then mUI_VarsGlobalInit(version) end
	if(mUI_CVARS == nil) then mUI_VarsCharacterInit(version) end

	--if(mUI_GVARS["Version"] ~= version) then
	--	local c1 = color2chat(MUI_CFG_COLOR1)
	--	cprint("|cffff0000WARNING!|r Outdated config version detected.\n" ..
	--	       "Visit |cff6666eehttps://gitlab.com/byte_shifter/minimalUI|r for help!")
	--ende
	
	local w, h = wapiGetResolution()
	local gui_width  = mui_range(w * 0.33, 500, 1000)
	local gui_height = mui_range(h * 0.33, 400, 800)


	MUI_CONFIG_FRAME = uiCreateTopLevelFrame(UIParent, "ConfigFrame", gui_width, gui_height)
	MUI_CONFIG_FRAME:SetFrameStrata("DIALOG")
	MUI_CONFIG_FRAME:SetPoint("CENTER", UIParent, "CENTER")
	MUI_CONFIG_FRAME:SetShadowColor(0,0,0,0)
	uiFrameRegisterDragging(MUI_CONFIG_FRAME)

	MUI_CONFIG_FRAME.title = uiFontString(MUI_CONFIG_FRAME)
	MUI_CONFIG_FRAME.title:SetPoint("TOPLEFT", MUI_CONFIG_FRAME, "TOPLEFT", 0, 0)
	MUI_CONFIG_FRAME.title:SetPoint("BOTTOMRIGHT", MUI_CONFIG_FRAME, "TOPRIGHT", 0, -18)
	MUI_CONFIG_FRAME.title:SetText(mui_color2chat(MUICOLORS[5]).."minimalUI|r Configuration |cFF888888v"..version)

	if(false) then
		-- Display debug informations in the title bar!
		MUI_CONFIG_FRAME.dtt = 1.0
		MUI_CONFIG_FRAME:SetScript("OnUpdate", function (this, arg1)
			local dt = arg1
			this.dtt = this.dtt + dt
			if(this.dtt > 1.0) then
				this.dtt = 0
				local _,__,ping = GetNetStats()
				local debuginfo = string.format(" | MEMORY: %.2fmb, FPS: %i, LATENCY: %ims", gcinfo()/1024, GetFramerate(), ping)
				this.title:SetText("minimalUI|r Configuration " .. version .. debuginfo)
			end
		end)
	end

	MUI_CONFIG_FRAME.close = uiCreateButton(MUI_CONFIG_FRAME, "Close", 96, 16, "Close")
	MUI_CONFIG_FRAME.close:SetPoint("BOTTOMRIGHT", MUI_CONFIG_FRAME, "BOTTOMRIGHT", -6, 6)
	MUI_CONFIG_FRAME.close:SetScript("OnClick", function() 
		if(MUI_CONFIG_FRAME.rr == 1) then
			uiPopupDialog(POPUP_YES_NO, "** Reload Required **", 
			"The user interface requires a reload.\n\nDo you want to reload now?", 
			function() ReloadUI() end, nil)
		else
			MUI_CONFIG_FRAME:Hide() 
		end
	end)

	MUI_CONFIG_FRAME.unlock = uiCreateToggleButton(MUI_CONFIG_FRAME, "Unlock", 96, 16, "Unlock", function(this) 
		 if(uiGetGlobalDragState()) then
			this:SetState(false)
			uiEnableGlobalDragging(false)
			this:SetText("Unlock")
		else
			this:SetState(true)
			uiEnableGlobalDragging(true)
			this:SetText("Lock")
		end
	end)
	MUI_CONFIG_FRAME.unlock.colors[1] = {0.2, 0.2, 0.2, 1.0}
	MUI_CONFIG_FRAME.unlock.colors[2] = {0.1, 0.6, 0.1, 1.0}
	MUI_CONFIG_FRAME.unlock:SetPoint("BOTTOMRIGHT", MUI_CONFIG_FRAME, "BOTTOMRIGHT", -107, 6)
	MUI_CONFIG_FRAME.unlock:SetState(uiGetGlobalDragState())

	MUI_CONFIG_FRAME.hoverbind = uiCreateButton(MUI_CONFIG_FRAME, "Hoverbind", 96, 16, "Hoverbind")
	MUI_CONFIG_FRAME.hoverbind:SetBackgroundColor(.8, 0.4, 0.1, 1.0)
	MUI_CONFIG_FRAME.hoverbind:SetPoint("BOTTOMRIGHT", MUI_CONFIG_FRAME, "BOTTOMRIGHT", -208, 6)
	MUI_CONFIG_FRAME.hoverbind:SetScript("OnClick", function() uiHoverbindToggle() end)

	MUI_CONFIG_LAST_BTN = nil
	local view_btn_handler = function(this)
		local mod = this.content_ref
		if(mod) then
		   	local last_mod = MUI_CONFIG_FRAME.scrollframe.last_content
		   	if(last_mod and last_mod ~= mod) then
		   		if(last_mod.config) then last_mod.config:Hide() end
		   		if(last_mod.OnConfigHide) then last_mod:OnConfigHide() end
		   	end

			if(mod.OnConfigShow) then
				mod:OnConfigShow() 
			end

			MUI_CONFIG_FRAME.scrollframe:SetScrollChild(mod.config)
			MUI_CONFIG_FRAME.scrollframe.last_content = mod
			
			if(MUI_CONFIG_LAST_BTN) then
				if(MUI_CONFIG_LAST_BTN ~= this) then
					MUI_CONFIG_LAST_BTN:SetState(not MUI_CONFIG_LAST_BTN.toggled)
				end
			end
			
			if(MUI_CONFIG_LAST_BTN ~= this) then
				this:SetState(not this.toggled)
			end
			
			MUI_CONFIG_LAST_BTN = this
		end
	end

	MUI_CONFIG_FRAME.scrollframe = uiCreateScrollFrame(MUI_CONFIG_FRAME)
	MUI_CONFIG_FRAME.scrollframe:SetPoint("TOPLEFT", MUI_CONFIG_FRAME, "TOPLEFT", 136, -21)
   	MUI_CONFIG_FRAME.scrollframe:SetPoint("BOTTOMRIGHT", MUI_CONFIG_FRAME, "BOTTOMRIGHT", -4 - 12, 32)
   	MUI_CONFIG_FRAME.scrollframe.last_content = nil
	MUI_CONFIG_FRAME.scrollframe:SetBackgroundColor(0.0,0.0,0.0,0.0)
	MUI_CONFIG_FRAME.scrollframe:SetBorderColor(0.0,0.0,0.0,0.0)
	MUI_CONFIG_FRAME.scrollframe:SetShadowColor(0.0,0.0,0.0,0.0)

	local function add_button_and_content(module, last_btn)
		local content = uiCreateElement(MUI_CONFIG_FRAME.scrollframe, "Frame", module.name, gui_width - 136 - 16)
		content:SetBackgroundColor(0.0,0.0,0.0,0.0)
		
		local button = uiCreateToggleButton(MUI_CONFIG_FRAME, module.name, 128, 16, module.name, view_btn_handler)
		button.content_ref = module
		if(last_btn) then
			button:SetPoint("TOPLEFT", last_btn, "TOPLEFT", 0, -18)
		else
			button:SetPoint("TOPLEFT", MUI_CONFIG_FRAME, "TOPLEFT", 4, -21)
		end
		module.config = content
		return button
	end

   	local last_btn = nil
   	for idx, module in pairs(mUI.modules_sorted) do
   		last_btn = add_button_and_content(module, last_btn)
   	end	

  	MUI_CONFIG_FRAME:Hide()
end

function mUI_VarInitModule(module, name, defval)
	if(mUI_CVARS[module] == nil) then
		mUI_CVARS[module] = {}
	end

	if(MUI_VARS_TABLE[module] == nil) then
		MUI_VARS_TABLE[module] = {}
	end

	if(mUI_CVARS[module][name] == nil and defval ~= nil) then
		mUI_CVARS[module][name] = defval
	end

	return mUI_CVARS[module][name]
end

function mUI_VarRegister(module, frame, onvarchange)
	if(frame == nil) then 
		cerror("Frame is nil!") 
	end

	if(MUI_VARS_TABLE[module] == nil) then 
		cerror("Variable for module ".. module .." was never created!") 
	end

	if(frame.OnVarChange == nil) then 
		frame.OnVarChange = onvarchange 
	end
	
	table.insert(MUI_VARS_TABLE[module], frame)

	for name, value in pairs(mUI_CVARS[module]) do
		frame:OnVarChange(name, value)
	end
end

function mUI_SetReloadRequired()
	if(mUI.initialized) then MUI_CONFIG_FRAME.rr = 1 end
end

function mUI_VarSetValue(module, name, value)
	if(mUI_CVARS[module] == nil) 
	then
		cerror(module.." no vars exisiting yet!")
		return 
	end
	
	mUI_CVARS[module][name] = value
	if(name == "Enabled") 
	then
		ReloadUI()
		--[[
		local mod = mUI.modules[module]
		if(value) then
			if(not mod.enabled) then
				mod.enabled = true
				mod:OnEnable()
				mod.loaded = true
			end
		else 
			if(mod.enabled) then
				mod.enabled = false
				mod:OnDisable()
				mod.loaded = false
			end
		end
		]]--
	else
		if(MUI_VARS_TABLE[module] ~= nil) 
		then
			for i, cl in pairs(MUI_VARS_TABLE[module]) 
			do 
				cl:OnVarChange(name, value) 
			end
		end
	end
	return value
end

function mUI_VarGetValue(module, name)
	if(mUI_CVARS[module] ~= nil and mUI_CVARS[module] ~= nil) 
	then
		return mUI_CVARS[module][name]
	else
		return mPreset_minimalUI[module][name]
		--cprint("Invalid variable: ".. module.. ">" .. name .. "!")
		--return nil
	end	
end

-- var ui elements
function mUI_VarGenBegin(frame, module, no_disable)
	if(frame == nil) then 
		cerror("frame is nil") 
	end

	frame.module = module
	frame.calc_height = 0
	frame.cursor_offsetx = 0
	frame.cursor_offsety = 0

	local enabled = true
	if(no_disable == nil) then
		enabled = mUI_VarGetValue(module, "Enabled")
		mUI_VarGenBeginGroup(frame, "Module Settings", enabled and {0.2, 0.6, 0.2, 1} or {0.467, 0.533, 0.600, 1.0} )
		mUI_VarGenBoolean(frame, "Enabled", nil, "Enables or disables the module.\n\n|cffffff00** CHANGING THIS VALUE WILL RELOAD THE UI!")
		mUI_VarGenButton(frame, "Reset", function () 
            --Todo: Make a question dialog box and ask if the user really wants to reset!!!!!
            uiPopupDialog(POPUP_YES_NO, "Reset settings?", "Do you want to reset all settings for module |cffffff00" .. module .. "|r?", 
				function() 
					mUI_CVARS[module] = {} 
					ReloadUI() 
				end) 
		end, "Reset Module Settings", "|cffffff00** THIS WILL RESET ALL SETTINGS IN THIS MODULE!", {0.2,0.2,0.2,1.0})

		local profiles, num_profiles = wapiGetProfilesForModule(module)
		if(num_profiles > 0) then
			mUI_VarGenComboButton(frame, "Select", profiles, function(value) 
			    uiPopupDialog(POPUP_YES_NO, "Load Custom Profile", "Do you want to load all settings from profile\n|cff00ffff".. tostring(value) .."|r for module |cffffff00" .. module .. "|r?", 
					function() 
						mUI_CVARS[module] = mUI_GVARS["Profiles"][value][module] 
						ReloadUI() 
					end) 
			end, "Copy Module settings from:", "Allows you to copy module specific settings from other profiles!\n\n|cffffff00** CHANGING THIS VALUE WILL RELOAD THE UI!", {0.8, 0.7, 0.2, 1.0})
		end
		mUI_VarGenEndGroup(frame)

		if(enabled == false) then 
			mUI_VarGenEnd(frame)
		end
	end
	return enabled
end

function mUI_VarGenBeginGroup(frame, name, bgcolor)
	if(frame.last_group == nil) 
	then
		
		frame.last_group = name

		local label = uiCreateElement(frame, "Frame", "Group", frame:GetWidth(), 12)
		if(bgcolor) 
		then
			label:SetBackgroundColor(unpack(bgcolor))
		else
			if not frame.cc then frame.cc = 1 else frame.cc = frame.cc + 1 end
			label:SetBackgroundColor(unpack(MUICOLORS[frame.cc]))
		end
		label:SetPoint("TOPLEFT", frame, "TOPLEFT", frame.cursor_offsetx , -frame.cursor_offsety)
		label.text = uiFontString(label, " " .. name, nil, nil, nil, nil, "CENTER", "LEFT", 1)

		frame.cursor_offsety = frame.cursor_offsety + 12 + 1
		frame.calc_height = frame.cursor_offsety
	end
end

function mUI_VarGenEndGroup(frame)
	frame.last_group = nil
	frame.cursor_offsety = frame.cursor_offsety + 4
end

function mUI_VarGenEnd(frame)
	frame:SetHeight(frame.calc_height)
	frame.init = true
end

MUI_VAR_LAST_FOCUS = nil
function mUI__ClearFocus()
	if(MUI_VAR_LAST_FOCUS) 
	then 
    	MUI_VAR_LAST_FOCUS:ClearFocus() 
	end
end
function mUI__SetFocus(editbox)
	if(editbox) 
	then
		MUI_VAR_LAST_FOCUS = editbox
    	MUI_VAR_LAST_FOCUS:HighlightText() 
    end
end

local function mUI__VarGenLabel(frame, width, itemvar, detailed, tooltip)
	local label = uiCreateElement(frame, "Frame", "Label"..itemvar, frame:GetWidth() - width - 1, 16)
	label:SetBackgroundColor(0.03, 0.03, 0.03, 0.9)
	label:SetPoint("TOPLEFT", frame, "TOPLEFT", frame.cursor_offsetx , -frame.cursor_offsety)
	if(tooltip) 
	then
		label:EnableMouse(true)
		label:SetScript("OnEnter", function (this)
			GameTooltip:SetOwner(this, "ANCHOR_CURSOR");
			GameTooltip:SetPoint("BOTTOMLEFT", this, "TOPLEFT", 0, 2)
    		GameTooltip:SetText("|cffffffff"..tooltip)
    		GameTooltip:Show()
		end)
		label:SetScript("OnLeave", function (self)
	   		GameTooltip:Hide()
		end)
	end

	label.text = uiFontString(label, " |cffcccccc"..(detailed or itemvar), nil, nil, "OUTLINE", nil, "CENTER", "LEFT")
	return label
end

local function mUI__VarGenCombo(frame, name, combo_type, combo_values, combo_callback)
	-- initialze a global set of dropdown menu items
	MUI_COMBOBOX_MENU_INIT() 
	
	
	--local combobox = CreateFrame("Button", frame:GetName().."Combobox"..name, frame)
	local combobox = uiCreateButton(frame, frame:GetName().."Combobox"..name, 94, 16)
	combobox.SetTextFit = function (self, text)
		local trimmed = false
		self.text:SetText(text)
		while(self:GetTextWidth() > self:GetWidth()-32) 
		do
			trimmed = true
			text = strsub(text, 1, strlen(text)-1)
			self.text:SetText(text)
		end
		if(trimmed) 
		then 
			self:SetText(text .. "..") 
		end
	end
	
	combobox.callback   = combo_callback
	combobox.combo_type = combo_type
	combobox.SetValueHeight = function (self, height)
		self.value_height = height
	end
	combobox:SetValueHeight(16)
	combobox:SetScript("OnClick", function (this)
	    mUI__ClearFocus()
		local r,g,b,a = this:GetBackgroundColor()
		MUI_COMBO_SET_BACKGROUNDCOLOR(r,g,b,a)
		MUI_COMBO_SET_CALLBACK(this, this.callback)
		
		if(this.combo_type == "FONT") 
		then
			MUI_COMBO_SET_VALUES(this, MUI_FONTS, 2)
		elseif(this.combo_type == "TEXTURE") 
		then
			MUI_COMBO_SET_VALUES(this, MUI_TEXTURES, 1)
		elseif(this.combo_type == "ENUM") 
		then
			MUI_COMBO_SET_VALUES(this, combo_values, 0)
		end
		MUI_COMBO_SHOW(this)
	end)

	combobox:SetHighlightTexture(highlight)
   	combobox:EnableMouse(true)
	
	combobox:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)
	return combobox
end

function mUI_VarGenButton(frame, text, onclick, detailed, tooltip, custom_color)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local label = mUI__VarGenLabel(frame, 95, "", detailed, tooltip)
	
	local button = uiCreateButton(frame, nil, 94, 16, text)
	if(custom_color) then
		button:SetBackgroundColor(unpack(custom_color))
	else
		button:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)
	end
	button:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	button:SetScript("OnClick", function ()
		mUI__ClearFocus()
		onclick()
	end)
	
	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety	
end

function mUI_VarGenBoolean(frame, itemvar, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 33, itemvar, detailed, tooltip)
	local checkbutton = uiCreateToggleButton(frame, itemvar, 32, 16, value and "On" or "Off", function (this)
		mUI__ClearFocus()
		value = mUI_VarSetValue(frame.module, itemvar, not this.toggled)
		this:SetState(value)
		this:SetText(value and "On" or "Off")
	end)
	
	checkbutton:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	checkbutton.colors[1] = {0.2, 0.8, 0.2, 0.9}
	checkbutton.colors[2] = {0.8, 0.2, 0.2, 0.9}
	checkbutton:SetState(value)
	--checkbutton:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)
	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety
end

function mUI_VarGenComboButton(frame, text, itemenums, onclick, detailed, tooltip, custom_color)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local label = mUI__VarGenLabel(frame, 95, text, detailed, tooltip)
	local comboclb = function(self, index, value) 
		onclick(value)
	end
	
	local combobox = mUI__VarGenCombo(frame, "", "ENUM", itemenums, comboclb)
	combobox:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	combobox:SetText(text)
	if(custom_color) then
		combobox:SetBackgroundColor(unpack(custom_color))
	else
		combobox:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)
	end


	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety	
end

function mUI_VarGenEnumeration(frame, itemvar, itemenums, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 95, itemvar, detailed, tooltip)
	local comboclb = function(self, index, value) 
		mUI_VarSetValue(frame.module, itemvar, value)
		self:SetText(value) 
	end
	local combobox = mUI__VarGenCombo(frame, itemvar, "ENUM", itemenums, comboclb)
	combobox:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	combobox:SetWidth(95)
	combobox:SetText(value)
	combobox:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)
	
	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety	
end

function mUI_VarGenEnumTextures(frame, itemvar, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 95, itemvar, detailed, tooltip)
	local comboclb = function (self, index, value)
		local texture_full = value
		if(texture_full) then
			mUI_VarSetValue(frame.module, itemvar, texture_full)
			self:SetNormalTexture(texture_full)
			
			local _,_,texture = strfind(texture_full, "([^\\]+)[.]") 
			if(texture) then
				self:SetTextFit(texture)
			end
		end
	end
	local combobox = mUI__VarGenCombo(frame, itemvar, "TEXTURE", nil, comboclb)
	combobox:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	combobox:SetWidth(95)
	local _,_,texfile = strfind(value, "([^\\]+)[.]") 
	if(texfile) then combobox:SetTextFit(texfile) end
	combobox:SetNormalTexture(value)
	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety	
end

function mUI_VarGenEnumFonts(frame, itemvar, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 95, itemvar, detailed, tooltip)
	local comboclb = function (self, index, value)
		local fontfull = value
		if(fontfull) then
			mUI_VarSetValue(frame.module, itemvar, fontfull)
			self:SetFont(fontfull, MUI_CONFIG_FONT_SIZE-3)
			local _,_,fontfile = strfind(fontfull, "([^\\]+)[.]") 
			if(fontfile) then
				self:SetTextFit(fontfile)
			end
		end
	end
	
	local combobox = mUI__VarGenCombo(frame, itemvar, "FONT", nil, comboclb)
	combobox:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	combobox:SetFont(value, MUI_CONFIG_FONT_SIZE-3)
	combobox:SetWidth(95)
	local _,_,fontfile = strfind(value, "([^\\]+)[.]") 
	if(fontfile) then combobox:SetTextFit(fontfile) end

	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety		
end

function mUI_VarGenTextField(frame, itemvar, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 220, itemvar, detailed, tooltip)
	
	local editbox = uiCreateTextEdit(frame, itemvar, value, 220, 16)
	editbox:SetScript("OnEscapePressed", function() mUI__ClearFocus() end)
	editbox:SetScript("OnEnterPressed", function() 
		mUI_VarSetValue(frame.module, itemvar, this:GetText())
		mUI__ClearFocus()
	end)
	editbox:SetScript("OnEditFocusGained", function() mUI__SetFocus(this) end)
   	editbox:SetScript("OnEditFocusLost", function() this:HighlightText(0,0) end)
   	editbox:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	editbox:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)

   	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety	
end

function mUI_VarGenNumberField(frame, itemvar, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 95, itemvar, detailed, tooltip)

	local editbox = uiCreateNumberEdit(frame, itemvar, value, 95, 16)
	editbox:SetScript("OnEscapePressed", function(this) mUI__ClearFocus() end)
	editbox:SetScript("OnEnterPressed", function(this) 
		mUI_VarSetValue(frame.module, itemvar, this:GetNumber())
		mUI__ClearFocus()
	end)
	editbox:SetScript("OnEditFocusGained", function(this) mUI__SetFocus(this) end)
   	editbox:SetScript("OnEditFocusLost", function(this) this:HighlightText(0,0) end)
   	editbox:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	editbox:SetBackgroundColor(0.08, 0.08, 0.08, 0.9)

   	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety	
end

function mUI_VarGenColor(frame, itemvar, detailed, tooltip)
	if(frame == nil) then cerror("frame is nil") end
	local req_height = 16

	local req_height = 16
	local value = mUI_VarGetValue(frame.module, itemvar)
	local label = mUI__VarGenLabel(frame, 112, itemvar, detailed, tooltip)
	
	local color = mUI_VarColorFromValue(value)
	
	local function fn(self, r, g, b, a) 
		local c = { r,g,b,a }
		mUI_VarSetValue(frame.module, itemvar,  mUI_VarValueFromColor(c))
		self:SetBackgroundColor(r,g,b,1.0)
		self.coloredit:SetText(mui_colortohexstr(c))
	end
	
	local picker = uiCreateColorPickerRGBA(frame, itemvar, 16, 16, fn, fn)
	picker:SetPoint("TOPLEFT", label, "TOPRIGHT", 1, 0)
	picker:SetBackgroundColor(unpack(color))

   	local editbox = uiCreateTextEdit(frame, itemvar, mui_colortohexstr(color), 95, 16)
	editbox:SetScript("OnEscapePressed", function() mUI__ClearFocus() end)
	editbox:SetScript("OnEnterPressed", function(self) 
	    local c = mui_hexstrtocolor(self:GetText())
	    if(c) then
			mUI_VarSetValue(frame.module, itemvar, mUI_VarValueFromColor(c))
			picker:SetBackgroundColor(unpack(c))
		end
		mUI__ClearFocus()
	end)
	editbox:SetScript("OnEditFocusGained", function(self) mUI__SetFocus(self) end)
   	editbox:SetScript("OnEditFocusLost", function(self) self:HighlightText(0,0) end)
   	editbox:SetPoint("TOPLEFT", picker, "TOPRIGHT", 1, 0)
	picker.coloredit    = editbox

   	frame.cursor_offsety = frame.cursor_offsety + req_height + 1
	frame.calc_height = frame.cursor_offsety		
end
