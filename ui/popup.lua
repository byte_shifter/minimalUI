local MUI_STATIC_POPUP_DIALOG = nil

-- dialogtype:
POPUP_OK 				= 0
POPUP_OK_CANCEL 		= 1
POPUP_YES_NO 			= 2
POPUP_YES_NO_CANCEL 	= 3
function uiPopupDialog(dialogtype, titlemsg, message, clb1, clb2, clb3) -- varargs are callbacks to the pressed button correspondig to the dialogtype
	if(MUI_STATIC_POPUP_DIALOG == nil ) then  
		local frame = uiCreateElement(UIParent, "Frame", "StaticPopupFrame", 400)
		frame:SetPoint("CENTER", UIParent, "CENTER")
		frame:SetFrameStrata("DIALOG")
		frame:SetFrameLevel(3)
		frame:SetBackgroundColor(unpack(UI_DEFAULT_BG_COLOR))
		frame:SetBorderColor(0.7, 0.7, 0.2, 0.9)
		--uiAddThinBorder(frame, unpack(UI_DEFAULT_FG_COLOR1), true, true, true, true)
		uiFrameRegisterDragging(frame)
	
		frame.title = uiFontString(frame)
		frame.title:ClearAllPoints()
		frame.title:SetPoint("TOP", frame, "TOP", 0, -2)
	
		frame.text = uiFontString(frame)
		frame.text:ClearAllPoints()
		frame.text:SetPoint("TOP", frame, "TOP", 0, -24)

		frame.btn1 = uiCreateButton(frame, "_BTN1", 96, 16)
		frame.btn1:Hide()
		frame.btn2 = uiCreateButton(frame, "_BTN2", 96, 16)
		frame.btn2:Hide()
		frame.btn3 = uiCreateButton(frame, "_BTN3", 96, 16)
		frame.btn3:Hide()
		frame:Hide()

		MUI_STATIC_POPUP_DIALOG = frame
	end

	if(MUI_STATIC_POPUP_DIALOG:IsVisible()) then
		cdebug("Only one popup window at a time is supported!")
		return
	end

	MUI_STATIC_POPUP_DIALOG.btn1:SetScript("OnClick", nil)
	MUI_STATIC_POPUP_DIALOG.btn2:SetScript("OnClick", nil)
	MUI_STATIC_POPUP_DIALOG.btn3:SetScript("OnClick", nil)

	MUI_STATIC_POPUP_DIALOG.title:SetText("|cffffff00"..titlemsg)
	MUI_STATIC_POPUP_DIALOG.text:SetText(message)

	if(dialogtype == 0) then
		MUI_STATIC_POPUP_DIALOG.btn1:SetPoint("BOTTOM", MUI_STATIC_POPUP_DIALOG, "BOTTOM", 0, 12)
		MUI_STATIC_POPUP_DIALOG.btn1:SetText("Ok")
		MUI_STATIC_POPUP_DIALOG.btn1:SetScript("OnClick", function(this, arg1) MUI_STATIC_POPUP_DIALOG:Hide() if(arg1 and clb1) then clb1() end end)
		MUI_STATIC_POPUP_DIALOG.btn1:Show()
		MUI_STATIC_POPUP_DIALOG.btn2:Hide()
		MUI_STATIC_POPUP_DIALOG.btn3:Hide()
	elseif(dialogtype == 1 or dialogtype == 2) then
		MUI_STATIC_POPUP_DIALOG.btn1:SetPoint("BOTTOM", MUI_STATIC_POPUP_DIALOG, "BOTTOM", -53, 12)
		if(dialogtype == 1) then
			MUI_STATIC_POPUP_DIALOG.btn1:SetText("Ok")
		else
			MUI_STATIC_POPUP_DIALOG.btn1:SetText("Yes")
		end
		MUI_STATIC_POPUP_DIALOG.btn1:SetScript("OnClick", function(this, arg1, arg2) MUI_STATIC_POPUP_DIALOG:Hide() if(arg1 and clb1) then clb1() end end)
		MUI_STATIC_POPUP_DIALOG.btn1:Show()

		MUI_STATIC_POPUP_DIALOG.btn2:SetPoint("BOTTOM", MUI_STATIC_POPUP_DIALOG, "BOTTOM", 53, 12)
		if(dialogtype == 1) then
			MUI_STATIC_POPUP_DIALOG.btn2:SetText("Cancel")
		else
			MUI_STATIC_POPUP_DIALOG.btn2:SetText("No")
		end
		MUI_STATIC_POPUP_DIALOG.btn2:SetScript("OnClick", function(this, arg1) MUI_STATIC_POPUP_DIALOG:Hide() if(arg1 and clb2) then clb2() end end)
		MUI_STATIC_POPUP_DIALOG.btn2:Show()

		MUI_STATIC_POPUP_DIALOG.btn3:Hide()
	elseif(dialogtype == 3)  then
		MUI_STATIC_POPUP_DIALOG.btn1:SetPoint("BOTTOM", MUI_STATIC_POPUP_DIALOG, "BOTTOM", -101, 12)
		MUI_STATIC_POPUP_DIALOG.btn1:SetText("Yes")
		MUI_STATIC_POPUP_DIALOG.btn1:SetScript("OnClick", function(this, arg1) MUI_STATIC_POPUP_DIALOG:Hide() if(arg1 and clb1) then clb1() end end)
		MUI_STATIC_POPUP_DIALOG.btn1:Show()

		MUI_STATIC_POPUP_DIALOG.btn2:SetPoint("BOTTOM", MUI_STATIC_POPUP_DIALOG, "BOTTOM", 0, 12)
		MUI_STATIC_POPUP_DIALOG.btn2:SetText("No")
		MUI_STATIC_POPUP_DIALOG.btn2:SetScript("OnClick", function() MUI_STATIC_POPUP_DIALOG:Hide() if(arg1 and clb2) then clb2() end end)
		MUI_STATIC_POPUP_DIALOG.btn2:Show()

		MUI_STATIC_POPUP_DIALOG.btn3:SetPoint("BOTTOM", MUI_STATIC_POPUP_DIALOG, "BOTTOM", 101, 12)
		MUI_STATIC_POPUP_DIALOG.btn3:SetText("Cancel")
		MUI_STATIC_POPUP_DIALOG.btn3:SetScript("OnClick", function() MUI_STATIC_POPUP_DIALOG:Hide() if(arg1 and clb3) then clb3() end end)
		MUI_STATIC_POPUP_DIALOG.btn3:Show()
	end

	MUI_STATIC_POPUP_DIALOG:SetHeight(24 + MUI_STATIC_POPUP_DIALOG.text:GetHeight() + 38)
	MUI_STATIC_POPUP_DIALOG:Show()
end
