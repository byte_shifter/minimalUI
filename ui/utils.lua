-- Global frame dragging for unlocking every moveable ui element
local MUI_GLOBAL_FRAME_DRAG_ENABLED = false
MUI_GLOBAL_FRAME_DRAG_FRAMES 			= {}

function uiGetGlobalDragState()
	return MUI_GLOBAL_FRAME_DRAG_ENABLED
end

function uiEnableGlobalDragging(enabled)
	MUI_GLOBAL_FRAME_DRAG_ENABLED = enabled
	
	if(UIParent.grid_view == nil) then
		-- grid view
		UIParent.grid_view = uiCreateElement(UIParent, "Frame", "GridCellLayoutHelper")
		UIParent.grid_view:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0)
		UIParent.grid_view:SetPoint("BOTTOMRIGHT", UIParent, "BOTTOMRIGHT", 0, 0)
		UIParent.grid_view:SetFrameStrata("BACKGROUND")
		UIParent.grid_view:SetBackgroundColor(0.1,0.1,0.1,0.9)
					
		local function make_line(view, x, y, width, r, g, b, a)
			local w  = width + (1 - mod(width,2)) -- Make sure we have uneven widths so that we can have a center point
			local hw = math.floor(w * 0.50)
			local l  = view:CreateTexture(nil, "BORDER")
			local t  = (x == 0)
			l:SetColorTexture(r, g, b, a)
			l:SetAlpha(a)
			if(t) then
				l:SetPoint("TOPLEFT", UIParent, "TOPLEFT", x, -y + hw)
				l:SetHeight(width)
				l:SetWidth(view:GetWidth())
			else
				l:SetPoint("TOPLEFT", UIParent, "TOPLEFT", x - hw, -y)
				l:SetHeight(view:GetHeight())
				l:SetWidth(width)
			end
			return l
		end
		
		local function draw_grid(view, step, thickness, color)
			local sx = math.floor(view:GetWidth())
			local sy = math.floor(view:GetHeight())
			local shx = math.floor(sx * 0.5)
			local shy = math.floor(sy * 0.5)
		
			local linecolor = color
			local thickness = thickness
			local step_x = step
			local lox 	 = shx
			local rox 	 = shx
			make_line(view, rox, 0, thickness, unpack(linecolor))
			while(rox <= sx) do
				lox = lox - step_x
				rox = rox + step_x
				make_line(view, rox, 0, thickness, unpack(linecolor))
				make_line(view, lox, 0, thickness, unpack(linecolor))
			end
			make_line(view, shx, 0, thickness, unpack(linecolor))

			local step_y = step
			local loy 	 = shy 
			local roy 	 = shy
			make_line(view, 0, roy, thickness, unpack(linecolor))
			while(roy < sy) do
				loy = loy - step_y
				roy = roy + step_y
				make_line(view, 0, roy, thickness, unpack(linecolor))
				make_line(view, 0, loy, thickness, unpack(linecolor))
			end
			make_line(view, 0, shy, thickness, unpack(linecolor))
		end
		
		draw_grid(UIParent.grid_view, 32, 2.0, {0.0, 0.0, 0.0, .5})
		draw_grid(UIParent.grid_view, 16, 1.0, {0.0, 0.0, 0.0, .4})
	end

	if(enabled) then
		if(not UIParent.grid_view:IsVisible()) then
			UIParent.grid_view:Show()
		end
	else
		if(UIParent.grid_view:IsVisible()) then
			UIParent.grid_view:Hide()
		end
	end

	-- notify every module that global lock/unlock was triggered
	for i, module in pairs(mUI.modules_sorted)
	do
		if(module.enabled) then
			if(enabled) then
				module:OnGlobalUnlock()
			else
				module:OnGlobalLock()
			end
		end
	end

	-- frame drag enable/disable
	for idx, frame in pairs(MUI_GLOBAL_FRAME_DRAG_FRAMES) do
		if(enabled) then 
			frame.locked = false
			frame.dragpanel:Show()
		else
			frame.locked = true
			frame.dragpanel:Hide()
		end
	end
end

function uiFrameRegisterDragging(frame, lock_state)
	if(frame.locked == nil)
	then
		frame.locked = lock_state or false
		frame:EnableMouse(true)	
		frame:SetClampedToScreen(true)
		frame:SetMovable(true)
		frame:RegisterForDrag("LeftButton")
		
		frame:HookScript("OnDragStart", function(self) 
			if(not self.locked) 
			then 
				self:StartMoving() 
			end
		end)
		frame:HookScript("OnDragStop",  function(self) 
			if(not self.locked) 
			then 
				self:StopMovingOrSizing()
			end	
		end)
	end
end

function uiFrameRegisterGlobalDragging(frame)
	if(not mui_tcontains(MUI_GLOBAL_FRAME_DRAG_FRAMES, frame)) then
		table.insert(MUI_GLOBAL_FRAME_DRAG_FRAMES, frame)

		if(frame.dragpanel == nil) then
			uiFrameRegisterDragging(frame, true)
			
			frame.dragpanel = uiCreateElement(frame, "Frame", "DragPanel")
			frame.dragpanel:SetBackgroundColor(mui_unpack_color(MUICOLORS[5], .75))
			frame.dragpanel:SetFrameStrata("TOOLTIP")
			frame.dragpanel:SetFrameLevel(100)
			frame.dragpanel:SetAllPoints(frame)
			frame.dragpanel.shadow:Hide()

			frame.dragpanel.text = uiFontString(frame.dragpanel, "", nil, nil, nil, "OVERLAY", "TOP", "LEFT", 1)
   			frame.dragpanel.text:OffsetText(5, 5, 5, 5)
			frame.dragpanel:Hide()

   			frame.dragpanel:SetScript("OnUpdate", function(self) 
				local str = string.format("%s (%s)\nx:%i y:%i", frame:GetName(), frame:GetParent():GetName() or "n/a", 
			                          frame:GetLeft() or "-1", frame:GetTop() or "-1")
				self.text:SetText(str)

				-- If CTRL key is pressed, hide this layer
				if(IsControlKeyDown()) then
					self:SetAlpha(0)
				else
					self:SetAlpha(1)
				end
           	end)
        end
	end
end

function uiFrameUnregisterGlobalDragging(frame)
	local index = mui_tcontains(MUI_GLOBAL_FRAME_DRAG_FRAMES, frame)
	if(index) then
		table.remove(MUI_GLOBAL_FRAME_DRAG_FRAMES, index)
	end
end