local MUI_COMBOBOX_MENU               = nil
local MUI_COMBOBOX_MENU_NUM_ITEMS_MAX = 30

function MUI_COMBOBOX_MENU_INIT()
	if(MUI_COMBOBOX_MENU ~= nil) then return end
	
	MUI_COMBOBOX_MENU = uiCreateElement(UIParent, "Frame", "StaticComboBoxMenuFrame")
	MUI_COMBOBOX_MENU:SetFrameStrata("DIALOG")
	MUI_COMBOBOX_MENU:SetFrameLevel(5)
	MUI_COMBOBOX_MENU.dt = 0
	MUI_COMBOBOX_MENU:SetScript("OnUpdate", function (this, dt)
	    this.dt = this.dt + dt
	    if(this.dt > .5) then
	    	this.dt = 0
	    	local hover = (MouseIsOver(this) or MouseIsOver(this.PARENT))
	    	if(hover) then return end
	    	this:Hide()
	    end
	end)
	MUI_COMBOBOX_MENU.FIELDS = {}
	for index=1, MUI_COMBOBOX_MENU_NUM_ITEMS_MAX do
		MUI_COMBOBOX_MENU.FIELDS[index] = uiCreateButton(MUI_COMBOBOX_MENU, "Item"..index)
		MUI_COMBOBOX_MENU.FIELDS[index].index = index
		MUI_COMBOBOX_MENU.FIELDS[index]:SetScript("OnClick", function (this)
			if(MUI_COMBOBOX_MENU.CALLBACK and MUI_COMBOBOX_MENU._VALUES) then 
				local value = MUI_COMBOBOX_MENU._VALUES[this.index]
				MUI_COMBOBOX_MENU.CALLBACK(MUI_COMBOBOX_MENU.PARENT, this.index, value) 
			end
			MUI_COMBOBOX_MENU:Hide()
			MUI_COMBOBOX_MENU.dt = 0
		end)
		
		local highlight = MUI_COMBOBOX_MENU.FIELDS[index]:CreateTexture()
		highlight:SetPoint("TOPLEFT", MUI_COMBOBOX_MENU.FIELDS[index], "TOPLEFT", 1, -1)
		highlight:SetPoint("BOTTOMRIGHT", MUI_COMBOBOX_MENU.FIELDS[index], "BOTTOMRIGHT", -1, 1)
		highlight:SetColorTexture(.5, .5, .5, .1)
		MUI_COMBOBOX_MENU.FIELDS[index]:SetHighlightTexture(highlight)
	end
	MUI_COMBOBOX_MENU:Hide()
end

function MUI_COMBO_SET_VALUES(combobox, values, combo_type)
	if(MUI_COMBOBOX_MENU._VALUES == values) then return end
	if(combo_type == nil) then combo_type = MUI_COMBOBOX_MENU_TYPE_ENUM end
	
	MUI_COMBOBOX_MENU._VALUES = values
	local height = 0
	local max_width = 0
	local offset = -1	
	local r,g,b,a = combobox:GetBackgroundColor()
	for index, field in ipairs(MUI_COMBOBOX_MENU.FIELDS) do
		local value = values[index]
		if(values[index]) then
			field:SetPoint("TOPLEFT", MUI_COMBOBOX_MENU, "TOPLEFT", 1, offset)
			field:SetWidth(232)
			field.bg:SetTexture("")
			
			if(combo_type == 2) then
				-- fonts
				local _,_,fontfile = strfind(value, "([^\\]+)[.]") 
				if(fontfile) then field.text:SetText(fontfile) end
			elseif(combo_type == 1) then
				-- textures
				field.bg:SetTexture(value)
				local _,_,texturefile = strfind(value, "([^\\]+)[.]") 
				if(texturefile) then field:SetText(texturefile) end
			elseif(combo_type == 0) then
				-- enums (strings)
				field:SetText(value)
			end
			field:SetHeight(combobox.value_height)
			field:Show()

			local required_width = field:GetFontString():GetStringWidth() 
			if(required_width > max_width) then
				max_width = required_width
			end
			height = height + combobox.value_height
			offset = offset - combobox.value_height
		else
			field:Hide()
		end
	end	
	for index, field in ipairs(MUI_COMBOBOX_MENU.FIELDS) do
		field:SetWidth(max_width + 18) 
	end	
	MUI_COMBOBOX_MENU:SetHeight(height+2)
	MUI_COMBOBOX_MENU:SetWidth(max_width + 20)
end

-- Where callback gets this parameters passed CALLBACK(self, index, value)
function MUI_COMBO_SET_CALLBACK(parent, callback)
	MUI_COMBOBOX_MENU.CALLBACK = callback
	MUI_COMBOBOX_MENU.PARENT   = parent
end
function MUI_COMBO_SET_BACKGROUNDCOLOR(r,g,b,a)
	MUI_COMBOBOX_MENU:SetBackgroundColor(r,g,b,a-.05)
end
function MUI_COMBO_SHOW(cbx)
	MUI_COMBOBOX_MENU:SetPoint("TOPRIGHT", cbx, "TOPLEFT", -2, 0)
	--MUI_COMBOBOX_MENU:SetPoint("TOPRIGHT", cbx, "BOTTOMRIGHT")
	MUI_COMBOBOX_MENU:Show()
end




-- helpers

local function _UIGENNAME_(prefix, parent, postfix)
	local name = ""
	if(prefix) then name = prefix end
	if(parent and parent:GetName() and parent ~= UIParent) then name = name .. parent:GetName() end
	if(postfix) then name = name .. postfix end
	if(name == "") then name = nil end
	return name
end

--[[
~~~
ui api
~~~
]]--

function uiCreateElement(parent, type, name, width, height, template)
	local frame = CreateFrame(type, _UIGENNAME_(nil, parent, name), parent, template)
	frame:SetHeight(height or 200)
	frame:SetWidth(width or 300)
	
	frame.bg = frame:CreateTexture(_UIGENNAME_(nil, parent, "Background"), "BACKGROUND")
	frame.bg:SetAllPoints(frame)
	frame.bg:SetColorTexture(1,1,1,1)
	frame.bg:SetVertexColor(0.0, 0.0, 0.0, 1.0)
	frame.SetBackgroundColor = function(self, r, g, b, a) 
		self.bg:SetVertexColor(r, g, b, a)
		
		if(not self.bg:IsVisible()) then
			self.bg:Show()
		end
	end
	
	frame.GetBackgroundColor = function(self) 
		local r,g,b,a = self.bg:GetVertexColor()
		return r,g,b,a
	end
	
	frame.RegisterEvents = function(this, events) 
		if(events)
		then
			for i in pairs(events)
			do
        		this:RegisterEvent(events)
      		end
		end
	end
	
	frame.shadow = frame:CreateTexture(_UIGENNAME_(nil, parent, "Shadow"), "ARTWORK")
	frame.shadow:SetAllPoints(frame)
	frame.shadow:SetTexture("Interface\\AddOns\\minimalUI\\img\\Glow.tga")
	frame.SetShadowColor = function(self, r, g, b, a) 
		self.shadow:SetVertexColor(r, g, b, a)
		self.shadow:Show()
	end
	frame:SetShadowColor(0.0,0.0,0.0,1.0)
	
	frame.RegisterGlobalDragging 	= uiFrameRegisterGlobalDragging
	frame.UnregisterGlobalDragging 	= uiFrameUnregisterGlobalDragging
	frame:SetBackgroundColor(unpack(UI_DEFAULT_BG_COLOR))
	
	frame.border = {}
	for i=1, 4 do
		frame.border[i] = frame:CreateTexture(frame:GetName().."Border"..i, "OVERLAY")
		frame.border[i]:SetColorTexture(1,1,1,1)
	end
	
	frame.border[1]:SetPoint("TOPLEFT", frame, "TOPLEFT", 0.0, 0.0)
	frame.border[1]:SetPoint("BOTTOMRIGHT", frame, "TOPRIGHT", 0.0, -1.0)
	frame.border[2]:SetPoint("TOPLEFT", frame, "TOPLEFT", 0.0, -1.0)
	frame.border[2]:SetPoint("BOTTOMRIGHT", frame, "BOTTOMLEFT", 1.0, 1.0)
	frame.border[3]:SetPoint("TOPLEFT", frame, "BOTTOMLEFT", 0.0, 1.0)
	frame.border[3]:SetPoint("BOTTOMRIGHT", frame, "BOTTOMRIGHT", 0.0, 0.0)
	frame.border[4]:SetPoint("TOPLEFT", frame, "TOPRIGHT", -1.0, -1.0)
	frame.border[4]:SetPoint("BOTTOMRIGHT", frame, "BOTTOMRIGHT", 0.0, 1.0)

	frame.SetBorderColor = function (self, r, g, b, a)
		self.border[1]:SetVertexColor(r, g, b, a) self.border[1]:Show()
		self.border[2]:SetVertexColor(r, g, b, a) self.border[2]:Show()
		self.border[3]:SetVertexColor(r, g, b, a) self.border[3]:Show()
		self.border[4]:SetVertexColor(r, g, b, a) self.border[4]:Show()
	end
	
	frame:SetBorderColor(0.0, 0.0, 0.0, 1.0)	
	return frame
end

function uiCreateTopLevelFrame(parent, name, width, height, template)
	local frame = uiCreateElement(parent, "Frame", name, width, height, template)
	uiAddWindowBorder(frame)
	return(frame)
end

function uiCreateButton(parent, name, width, height, text, callback)
	if(parent == nil) then parent = UIParent end

	local button = uiCreateElement(parent, "Button", "Button".. (name or ""), width or 16, height or 16)
	button:SetNormalFontObject(MUIDEFAULTFONT)
	button:SetText(text)
		
	button.highlight = button:CreateTexture(button:GetName().."Highlight", "HIGHLIGHT")
	button.highlight:SetColorTexture(1, 1, 1, 1)
	button.highlight:SetPoint("TOPLEFT", button, "TOPLEFT", 1, -1)
	button.highlight:SetPoint("BOTTOMRIGHT", button, "BOTTOMRIGHT", -1, 1)
	button.SetHighlightColor = function (this, r, g, b, a)
		this.highlight:SetVertexColor(r, g, b, a)
	end
	button:SetHighlightColor(1.0, 1.0, 1.0, 0.08)
	
	button:SetScript("OnClick", function(this)
		if(this.callback) then this.callback(this) end
	end)
	
	button.callback = callback
	return button
end

function uiCreateToggleButton(parent, name, width, height, text, callback)
	local button    = uiCreateButton(parent, name, width, height, text, callback)
	button.colors   = { UI_DEFAULT_FG_COLOR3, UI_DEFAULT_FG_COLOR1 }
	button.toggled  = false
		
	button.SetState = function (this, state) 
		this.toggled = state
		if(this.toggled) then 
			this:SetBackgroundColor(unpack(this.colors[1]))
		else
			this:SetBackgroundColor(unpack(this.colors[2]))
		end
	end
	
	button:SetScript("OnClick", function(this) 
		if(this.callback) then 
			this.callback(this)
		else
			this:SetState(not this.toggled)
		end 
	end)

	return button
end

function uiCreateTextEdit(parent, name, text, width, height)
	local editbox = uiCreateElement(parent, "EditBox", (name or "") .. "TextEdit", width or 128, height or 16)
	editbox:SetAutoFocus(false)
	editbox:SetTextInsets(2,2,2,2)
	editbox:SetNumeric(false)
	editbox:SetScript("OnEscapePressed", function(this) this:ClearFocus() end)
	editbox:SetScript("OnEnterPressed", function(this) this:ClearFocus() end)
	editbox:SetText(text or "")
	editbox:SetTextColor(1,1,1,1)
	editbox:SetFontObject(MUIDEFAULTFONT)
	editbox:SetJustifyV("CENTER")
	editbox:SetJustifyH("RIGHT")
	
	editbox.highlight = editbox:CreateTexture(editbox:GetName().."Highlight", "HIGHLIGHT")
	editbox.highlight:SetColorTexture(1, 1, 1, 1)
	editbox.highlight:SetPoint("TOPLEFT", editbox, "TOPLEFT", 1, -1)
	editbox.highlight:SetPoint("BOTTOMRIGHT", editbox, "BOTTOMRIGHT", -1, 1)
	editbox.SetHighlightColor = function (self, r, g, b, a)
		self.highlight:SetVertexColor(r, g, b, a)
	end
	editbox:SetHighlightColor(1.0, 1.0, 1.0, 0.08)
	
	return editbox
end

function uiCreateNumberEdit(parent, name, number, width, height)
	local editbox = uiCreateElement(parent, "EditBox", (name or "") .. "NumberEdit", width or 128, height or 16)
	editbox:SetAutoFocus(false)
	editbox:SetTextInsets(4,2,2,2)
	editbox:SetNumeric(true)
	editbox:SetScript("OnEscapePressed", function(this) this:ClearFocus() end)
	editbox:SetScript("OnEnterPressed", function(this) this:ClearFocus() end)
	editbox:SetNumber(number or 0)
	editbox:SetTextColor(1,1,1,1)
	editbox:SetFontObject(MUIDEFAULTFONT)
	editbox:SetJustifyV("CENTER")
	editbox:SetJustifyH("RIGHT")
	
	editbox.highlight = editbox:CreateTexture(editbox:GetName().."Highlight", "HIGHLIGHT")
	editbox.highlight:SetColorTexture(1, 1, 1, 1)
	editbox.highlight:SetPoint("TOPLEFT", editbox, "TOPLEFT", 1, -1)
	editbox.highlight:SetPoint("BOTTOMRIGHT", editbox, "BOTTOMRIGHT", -1, 1)
	editbox.SetHighlightColor = function (self, r, g, b, a)
		self.highlight:SetVertexColor(r, g, b, a)
	end
	editbox:SetHighlightColor(1.0, 1.0, 1.0, 0.08)
	return editbox
end

-- Important: ScrollBar is within the ScrollFrame. Aligned to the right side of the frame.
function uiCreateScrollFrame(parent, name, width, height, thumb_width, thumb_height)
	local frame = uiCreateElement(parent, "ScrollFrame", "ScrollFrame"..(name or ""), width, height, "UIPanelScrollFrameTemplate")
	frame:SetBackgroundColor(0, 0, 0, 0.5)
	frame:EnableMouseWheel(1)
	
	-- Apply custom skin to blizzard interal scrollframe
	local bar = getglobal(frame:GetName().."ScrollBar")
	if(bar) then
		bar:ClearAllPoints()
		bar:SetPoint("TOPRIGHT", frame, "TOPRIGHT", 14, 0)
		bar:SetPoint("BOTTOMRIGHT", frame, "BOTTOMRIGHT", 14, 0)
		bar:SetWidth(12)
		--uiAddThinBorder(bar)
		frame.bar = bar
				
		local down = getglobal(bar:GetName().."ScrollDownButton")
		if(down) then
			down:EnableMouse(false)
			down:GetNormalTexture():SetTexture()
			down:GetPushedTexture():SetTexture()
			down:GetHighlightTexture():SetTexture()
			down:GetDisabledTexture():SetTexture()
		end
		
		local up = getglobal(bar:GetName().."ScrollUpButton")
		if(up) then
			up:EnableMouse(false)
			up:GetNormalTexture():SetTexture()
			up:GetPushedTexture():SetTexture()
			up:GetHighlightTexture():SetTexture()
			up:GetDisabledTexture():SetTexture()
		end
		
		local thumb = getglobal(bar:GetName().."ThumbTexture")
		if(thumb) then
			thumb:SetWidth(thumb_width or 12)
			thumb:SetHeight(thumb_height or 86)
			thumb:SetColorTexture(1,1,1,1)
			thumb:SetVertexColor(unpack(UI_DEFAULT_FG_COLOR1))
			frame.thumb = thumb
		end
	end
		
	-- Set custom scripts
	frame:SetScript("OnShow", function(this) 
	    -- set active and show scroll thumb is scrollchild is bigger than the scrollframe
	    local child = this:GetScrollChild()
	    if(child and child:GetHeight() > this:GetHeight()) then
			this.active = 1 
			this.thumb:Show() 
			--this.bar:ShowBorder(true)
		end
	end)
	frame:SetScript("OnHide", function(this) 
		this.active = 0 
		this.thumb:Hide() 
		--this.bar:ShowBorder(false)
	end)

   	frame:SetScript("OnMouseWheel", function(this, arg1)
        if(this.active) then
   			local stepsize	= 25
   			local current 	= this:GetVerticalScroll()
   			local max 	  	= this:GetVerticalScrollRange()
   			local direction = arg1 * -1
   			local new_pos	= mui_range(current + (stepsize * direction), 0, max)
   			this:SetVerticalScroll(new_pos)
   		end
   	end)
   	
	return frame
end

function uiFontString(parent, text, font, size, style, level, justifiyV, justifiyH, shadow)
	if(parent == nil) then return end

	local fontstring = parent:CreateFontString(nil, level)
	fontstring:SetAllPoints(parent)
	if(font and size and style) then
		fontstring:SetFont(font, size, style)
	else
		fontstring:SetFontObject(MUIDEFAULTFONT)
	end
   	fontstring:SetText(text or "")
	
	if(shadow) then
		fontstring:SetShadowColor(0,0,0,1)
		fontstring:SetShadowOffset(1,-1)
   	end

   	local valid_justifyV_values = { "TOP", "CENTER", "BOTTOM" }
   	if(mui_tcontains(valid_justifyV_values, justifiyV)) then
   		fontstring:SetJustifyV(justifiyV)
   	end
   	
   	local valid_justifyH_values = { "LEFT", "CENTER", "RIGHT" }
	if(mui_tcontains(valid_justifyH_values, justifiyH)) then
   		fontstring:SetJustifyH(justifiyH)
   	end

   	fontstring.OffsetText = function (this, left, right, top, bottom)
   		this:ClearAllPoints()
   		this:SetPoint("TOPLEFT", this:GetParent(), "TOPLEFT", left, -top)
   		this:SetPoint("BOTTOMRIGHT", this:GetParent(), "BOTTOMRIGHT", -right, bottom)
   	end

   	return fontstring
end

local function uiCreateColorPicker(parent, name, width, height, on_color, on_cancel, has_opacity)
	local colorpicker =	uiCreateButton(parent, "ColorPicker" .. (name or ""), width, height)
	
	colorpicker._func_   = on_color
	colorpicker._cancel_ = on_cancel
	
	colorpicker.tex_a = colorpicker:CreateTexture(nil, "OVERLAY")
	colorpicker.tex_a:SetPoint("TOPLEFT", colorpicker, "TOPRIGHT", -(width*.165), -1)
	colorpicker.tex_a:SetPoint("BOTTOMRIGHT", colorpicker, "BOTTOMRIGHT", -1, 1)
	colorpicker.tex_a:SetColorTexture(1,1,1,1)
	if(has_opacity) then
		colorpicker.tex_a:Show()
	else
		colorpicker.tex_a:Hide()
	end
	
	local function uiDefaultColorPickerFuncHandler()
		local r,g,b = ColorPickerFrame:GetColorRGB()
		local a = ColorPickerFrame.hasOpacity and OpacitySliderFrame:GetValue() or 1.0
		colorpicker.tex_a:SetVertexColor(a, a, a, 1.0)
		if(colorpicker._func_) then 
			colorpicker._func_(colorpicker, r, g, b, a)
		else
			colorpicker:SetBackgroundColor(r, g, b, 1.0)
		end
	end
	
	local function uiDefaultColorPickerOpacityHandler()
		local a = OpacitySliderFrame:GetValue()
		local r,g,b = ColorPickerFrame:GetColorRGB()
		if(colorpicker._func_) then 
			colorpicker._func_(colorpicker, r, g, b, a)
		end
	end
	
	local function uiDefaultColorPickerCancelHandler()
		if(colorpicker._cancel_) then 
			colorpicker._cancel_(colorpicker, unpack(colorpicker.previousValues))
		else
			colorpicker:SetBackgroundColor(unpack(colorpicker.previousValues))
		end
	end

	local function uiColorPickerButtonClickHandler(this)
		colorpicker.previousValues = {colorpicker.bg:GetVertexColor()};
		
		ColorPickerFrame.hasOpacity     = has_opacity or false
		ColorPickerFrame.opacity        = colorpicker.previousValues[4]
		ColorPickerFrame.func           = uiDefaultColorPickerFuncHandler
		ColorPickerFrame.cancelFunc     = uiDefaultColorPickerCancelHandler
		ColorPickerFrame.opacityFunc    = uiDefaultColorPickerFuncHandler
		
		ColorPickerFrame:ClearAllPoints()
		ColorPickerFrame:SetPoint("TOPLEFT", this, "BOTTOMLEFT", 2, 0)	
		ColorPickerFrame:SetColorRGB(unpack(colorpicker.previousValues));	
		ColorPickerFrame:Hide(); -- Need to run the OnShow handler.
		ColorPickerFrame:Show()
	end
	colorpicker.callback = uiColorPickerButtonClickHandler
	return(colorpicker)
end

function uiCreateColorPickerRGBA(parent, name, width, height, on_color, on_cancel)
	return(uiCreateColorPicker(parent, name, width, height, on_color, on_cancel, true))
end

function uiCreateColorPickerRGB(parent, name, width, height, on_color, on_cancel)
	return(uiCreateColorPicker(parent, name, width, height, on_color, on_cancel, false))
end


function uiCreateCombobox()

end
