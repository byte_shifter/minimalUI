mUI 				= CreateFrame("Frame", "minimalUI", UIParent)
mUI.modules 		= {}
mUI.modules_sorted 	= {}
mUI:RegisterEvent("PLAYER_ENTERING_WORLD")
mUI:RegisterEvent("ADDON_LOADED")
mUI:RegisterEvent("PLAYER_LOGOUT")
mUI:RegisterEvent("ADDON_ACTION_BLOCKED")
mUI:RegisterEvent("ADDON_ACTION_FORBIDDEN")
--mUI:RegisterEvent("COMBAT_TEXT_UPDATE")

SLASH_MUI1 = '/mui'
function SlashCmdList.MUI(msg, editbox)
	if MUI_CONFIG_FRAME then
  		if not MUI_CONFIG_FRAME:IsVisible() then
			MUI_CONFIG_FRAME:Show()
  		end
  	end
end

SLASH_RELOAD1 = '/rl'
function SlashCmdList.RELOAD(msg, editbox)
  ReloadUI()
end

SLASH_UNLOCK1 = '/unlock'
function SlashCmdList.UNLOCK(msg, editbox)
	if(not uiGetGlobalDragState()) then
		uiEnableGlobalDragging(true)
	end
end

SLASH_LOCK1 = '/lock'
function SlashCmdList.LOCK(msg, editbox)
	if(uiGetGlobalDragState()) then
		uiEnableGlobalDragging(false)
	end
end

SLASH_BIND1, SLASH_BIND2 = '/hoverbind', '/hb'
function SlashCmdList.BIND(msg, editbox)
	uiHoverbindToggle()
end

local function GameMenuFrame_AddConfigButton()
	local cfg_btn = CreateFrame("Button", "GameMenuButtonMUIConfig", GameMenuFrame, "GameMenuButtonTemplate")
	cfg_btn:SetPoint("TOP", 0, -38)
	cfg_btn:SetText("minimalUI")
	cfg_btn:SetScript("OnClick", function()
		HideUIPanel(GameMenuFrame)
	    if MUI_CONFIG_FRAME then 
			MUI_CONFIG_FRAME:Show() 
		end
	end)

	local point, relativeTo, relativePoint, xOffset, yOffset = GameMenuButtonContinue:GetPoint()
	GameMenuButtonContinue:Hide()
	cfg_btn:SetPoint(point, relativeTo, relativePoint, xOffset, yOffset)
end

local function derp()
	ActionButton_ShowGrid = function(button, reason) 
		button:SetAttribute("showgrid", 1)
		button:Show() 
	end
	ActionButton_HideGrid = function(button, reason) 
		button:SetAttribute("showgrid", 1)
		button:Show()
	end
	
	local function HideDefaultBlizzardStyle(btn)
		local r = {btn:GetRegions()}
		for i = 1, getn(r)-7 do
			local v = r[i]
			local rtype = v:GetObjectType()
			if rtype == "Texture" then
				if(i == 1) then
					v:SetTexCoord(.075, .925, .075, .925)
					v:SetDrawLayer("ARTWORK")
				end
				v:SetTexture(nil)
			elseif rtype == "FontString" then
				v:SetFont(UI_DEFAULT_FONT, UI_DEFAULT_FONTSIZE, UI_DEFAULT_FONTSTYLE)
			end
		end
	end

	local anchor = nil
	for i=1,12 do
		local btn = uiCreateElement(UIParent, "CheckButton", "abtn" .. i, nil, nil, "ActionBarButtonTemplate")
		if(anchor) then
			btn:SetPoint("TOPLEFT", anchor, "TOPRIGHT", 1, 0)
		else
			btn:SetPoint("CENTER", UIParent, "CENTER")
		end
		btn:SetWidth(32)
		btn:SetHeight(32)
		btn:SetID(i)
		
		iterate_regions(btn)
		btn.highlight = btn:CreateTexture(btn:GetName().."Highlight", "HIGHLIGHT")
		btn.highlight:SetColorTexture(1, 1, 1, 1)
		btn.highlight:SetPoint("TOPLEFT", btn, "TOPLEFT", 1, -1)
		btn.highlight:SetPoint("BOTTOMRIGHT", btn, "BOTTOMRIGHT", -1, 1)
		btn.SetHighlightColor = function (this, r, g, b, a)
			this.highlight:SetVertexColor(r, g, b, a)
		end
		btn:SetHighlightColor(1.0, 1.0, 1.0, 0.08)
		--btn:SetShadowColor(1,0,0,1)
		
		btn:SetAttribute("showgrid", 1)
		--ActionButton_UpdateAction(btn)
		--ActionButton_Update(btn);
		ActionButton_UpdateAction(btn);
		ActionButton_UpdateHotkeys(btn, btn.buttonType);
		
		HideDefaultBlizzardStyle(btn)
		anchor = btn
	end
end

mUI:SetScript("OnEvent", function(this, event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	--cdebugx({event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12})

	if(event == "ADDON_LOADED" and arg1 and arg1 == "minimalUI")
	then
		--NamePlateDriverFrame:Show()
		--[[
		getfenv(0)
		for i, v in ipairs() do
			cprint(i)
		end
		]]--
	
		SetConsoleKey("F7")
		GameMenuFrame_AddConfigButton()

		mUI_VarsInitialize(GetAddOnMetadata("minimalUI", "Version"))

		sort(this.modules_sorted, function(a,b) return a.priority > b.priority end)			
		for i, module in pairs(this.modules_sorted)
		do
			local IsEnabled = mUI_VarInitModule(module.name, "Enabled", true)
			if(IsEnabled) then 
				module.enabled = true
				module:OnEnable()
				module.loaded  = true
				mUI_VarRegister(module.name, module, HandleVarChange)
			else
				module:OnDisable()
			end
		end

		mUI.initialized = true
		cprint("Available Chat Commands:")
		cprint("|cffffff22/mui|r Shows minimalUI config menu")
		cprint("|cffffff22/unlock|r or |cffffff22/lock|r Unlocks or Locks all movable minimalUI frames")
		cprint("|cffffff22/hb|r or |cffffff22/hoverbind|r Toggles hoverbind mode to easily define hotkeys")

		uiHoverbindInitialize()
		
		--cprint(CharacterMicroButton:GetParent():GetName())
		
	else
		if(event == "ADDON_ACTION_BLOCKED" or event == "ADDON_ACTION_FORBIDDEN") then
			cdebugx({"Invalid addon interaction!", event, arg1, arg2, arg3})
--			cerror(event .. "|" .. (arg1 or "nil") .. "|" .. (arg2 or "nil") .. "|" .. (arg3 or "nil") .. "|" .. (arg4 or "nil") .. "|" .. (arg5 or "nil"))
			return
		end
		
		
		-- TODO: Get rid of the event system. Each frame can register to whatever events it wants to!!
		for name in pairs(this.modules)
		do
			local module = this.modules[name]
			if(module ~= nil)
			then
				for i, e in ipairs(module.events) do
					if(event == module.events[i])
					then
						if(module.enabled) then
							module:OnEvent(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12)
						end
					end
				end
			end	
		end
		
		if(event == "PLAYER_LOGOUT") then 
			wapiSaveCurrentProfile()
		end	
	end
end)

-- Registers a new module
function mUI:RegisterModule(name, priority)
	if(self.modules[name] == nil)
	then
		self.modules[name] 					= {}
		self.modules[name].enabled			= false
		self.modules[name].loaded			= false
		self.modules[name].priority 		= priority or PRIORITY_LOW
		self.modules[name].name 			= tostring(name)
		self.modules[name].events 			= {}
		self.modules[name].OnEnable 		= function() end -- stub function
		self.modules[name].OnDisable 		= function() ReloadUI() end -- stub function		                            		                 
		self.modules[name].OnEvent 			= function() end -- stub function
		self.modules[name].OnConfigShow		= function() end -- stub function
		self.modules[name].OnConfigHide		= function() end -- stub function
		self.modules[name].OnGlobalLock		= function() end -- stub function
		self.modules[name].OnGlobalUnlock	= function() end -- stub function
		self.modules[name].OnVarChange      = function() end -- stub function
		self.modules[name].RegisterEvents   = function(this, events)	
			if(events)
			then
				for i in pairs(events)
				do
	        		mUI:RegisterEvent(events[i])
	        		table.insert(self.modules[name].events, events[i])
	      		end
			end
		end
	
		-- insert module into a table which will be sorted by priority (highest > lowest)
		table.insert(self.modules_sorted, self.modules[name])
	end
	return self.modules[name]
end

-- Returns a already registered and loaded module
function mUI:GetModule(name)
	if(self.modules[name] and self.modules[name].loaded) then
		return self.modules[name]
	else 
		return nil
	end
end